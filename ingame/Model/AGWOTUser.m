//
//  AGWOTUser.m
//  ingame
//
//  Created by Eugene on 14/08/14.
//  Copyright (c) 2014 Eugene. All rights reserved.
//

#import "AGWOTUser.h"

AGWOTRegion* const kAGWOTRegionRU = @"ru";
AGWOTRegion* const kAGWOTRegionEU = @"eu";
AGWOTRegion* const kAGWOTRegionASIA = @"asia";
AGWOTRegion* const kAGWOTRegionNA = @"na";
AGWOTRegion* const kAGWOTRegionKR = @"kr";

NSString* const kAGWOTUserAccessToken       = @"kAGWOTUserAccessToken";
NSString* const kAGWOTUserAccountId         = @"kAGWOTUserAccountId";
NSString* const kAGWOTUserNickname          = @"kAGWOTUserNickname";
NSString* const kAGWOTUserLastBattleTime    = @"kAGWOTUserLastBattleTime";
NSString* const kAGWOTUserRegion            = @"kAGWOTUserRegion";
NSString* const kAGWOTUserGame              = @"kAGWOTUserGame";

@implementation AGWOTUser

+(BOOL)supportsSecureCoding {
    return YES;
}

-(id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self) {
        self.accountId      = [decoder decodeObjectOfClass:[NSString class]         forKey:kAGWOTUserAccountId];
        self.accessToken    = [decoder decodeObjectOfClass:[NSString class]         forKey:kAGWOTUserAccessToken];
        self.nickname       = [decoder decodeObjectOfClass:[NSString class]         forKey:kAGWOTUserNickname];
        self.lastBattleTime = [decoder decodeObjectOfClass:[NSDate class]           forKey:kAGWOTUserLastBattleTime];
        self.region         = [decoder decodeObjectOfClass:[kAGWOTUserRegion class] forKey:kAGWOTUserRegion];
        self.game           = [decoder decodeObjectOfClass:[NSString class]         forKey:kAGWOTUserGame];
    }
    return self;
}
-(void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.accountId        forKey:kAGWOTUserAccountId];
    [encoder encodeObject:self.accessToken      forKey:kAGWOTUserAccessToken];
    [encoder encodeObject:self.nickname         forKey:kAGWOTUserNickname];
    [encoder encodeObject:self.lastBattleTime   forKey:kAGWOTUserLastBattleTime];
    [encoder encodeObject:self.region           forKey:kAGWOTUserRegion];
    [encoder encodeObject:self.game             forKey:kAGWOTUserGame];
}
- (id) initWithAccessToken: (NSString*) accessToken andAccountId:(NSString *) accountId{
    self = [super init];
    self.accountId  = accountId;
    self.accessToken = accessToken;
    return self;
}

-(void)setNicknameWithNamespace:(NSString *)nicknameWithNamespace {
    NSArray* parts = [nicknameWithNamespace componentsSeparatedByString:@"_"];
    NSAssert(parts.count > 2, @"Nickname should be region_game_nickname (ex. ru_wot_derKetzer)");

    NSString* prefix    = [@[parts[0], parts[1]] componentsJoinedByString:@"_"];

    self.region     = parts[0];
    self.game       = parts[1];
    self.nickname   = [nicknameWithNamespace substringFromIndex:prefix.length];
}

-(NSString *)nicknameWithNamespace {
    return [@[self.region, self.game, self.nickname] componentsJoinedByString:@"_"];
}

-(BOOL)is_ingame {
    return (self.parseObject != nil) && [self.parseObject[@"active"] isEqual:@YES];
}

- (NSComparisonResult)compare:(AGWOTUser *)otherObject {
    return [self.nickname compare:otherObject.nickname options:NSCaseInsensitiveSearch];
}
-(NSArray *)getClanFriendsNicknames
{
    if (!self.clanInfo) return nil;
    NSDictionary *friendsDict = [self.clanInfo allValues][0][@"members"];
    NSMutableArray *result = [NSMutableArray array];
    for (NSDictionary *clanFriend in [friendsDict allValues]) {
        AGWOTUser *wotUser      = [AGWOTUser new];
        wotUser.nickname        = clanFriend[@"account_name"];
        wotUser.region          = self.region;
        wotUser.game            = @"wot";

        [result addObject:[wotUser nicknameWithNamespace]];
    }
    
    return result;
}

@end
