//
//  AGWOTUser.h
//  ingame
//
//  Created by Eugene on 14/08/14.
//  Copyright (c) 2014 Eugene. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NSString AGWOTRegion;

extern AGWOTRegion* const kAGWOTRegionRU;
extern AGWOTRegion* const kAGWOTRegionEU;
extern AGWOTRegion* const kAGWOTRegionASIA;
extern AGWOTRegion* const kAGWOTRegionNA;
extern AGWOTRegion* const kAGWOTRegionKR;

@interface AGWOTUser : NSObject<NSSecureCoding>

#pragma mark - Fields
@property (strong, nonatomic)   NSString*       accessToken;
@property (strong, nonatomic)   NSString*       accountId;
@property (strong, nonatomic)   NSNumber*       clanId;
@property (strong, nonatomic)   NSString*       nickname;
@property (strong, nonatomic)   NSDate*         lastBattleTime;
@property (strong, nonatomic)   NSDate*         token_expiresAt;
@property (strong, nonatomic)   PFObject*       parseObject;
@property (strong, nonatomic)   PFObject*       counter;
@property (strong, nonatomic)   AGWOTRegion*    region;
@property (strong, nonatomic)   NSString*       game;
//TODO: remove wotOnline?
@property (assign, nonatomic)   BOOL            wotOnline;
@property (strong, nonatomic)   id              clanInfo;
@property (strong, nonatomic)   NSArray*        friends;

#pragma mark - Dynamic fields
@property (readonly, nonatomic) BOOL      is_ingame;
/**
 *  Use it in all Parse backend operations
 */
@property (strong, nonatomic)   NSString* nicknameWithNamespace;

- (id) initWithAccessToken: (NSString*) accessToken andAccountId:(NSString *) accountId;
- (NSArray*) getClanFriendsNicknames;
@end
