//
//  AGGroupNameViewController.h
//  ingame
//
//  Created by Pavel Ivanov on 28/08/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SVProgressHUD.h>
#import "AGAddGroupContactCell.h"
#import "AGGroupNameTableViewCell.h"
#import "AGWOTUser.h"
#import "AGListViewController.h"
#import "AGWarGamingApiWorker.h"

typedef NS_ENUM(NSUInteger, AGSectionType) {
    AGSectionTypeNameStatic,
    AGSectionTypeFriendsIngame,
    AGSectionTypeFriendsNotIngame,
    AGSectionTypeEditActionStatic,
    AGSectionTypeDeleteActionStatic
};

@interface AGGroupNameViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSArray *groupContactsData;

@property (strong, nonatomic) NSDictionary *tableData;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSString *userTypedGroupName;
@property (strong, nonatomic) AGGameType *gameType;
@property (assign, nonatomic) AGListType listType;

- (IBAction)createButtonTap:(id)sender;
-(void)prepareDataForTable;

@end
