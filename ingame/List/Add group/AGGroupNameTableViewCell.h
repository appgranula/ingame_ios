//
//  AGGroupNameTableViewCell.h
//  ingame
//
//  Created by Pavel Ivanov on 28/08/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGGroupNameTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UITextField *groupName;

@end
