//
//  AGGroupNameTableViewCell.m
//  ingame
//
//  Created by Pavel Ivanov on 28/08/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import "AGGroupNameTableViewCell.h"

@implementation AGGroupNameTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)layoutSubviews
{
    UIColor *color = [UIColor colorWithRed:0.573 green:0.573 blue:0.514 alpha:0.690]    ;
    self.groupName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Group name", @"Group name")
                                                                           attributes:@{NSForegroundColorAttributeName: color}];
}

@end
