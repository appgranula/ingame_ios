//
//  AGGroupNameViewController.m
//  ingame
//
//  Created by Pavel Ivanov on 28/08/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import "AGGroupNameViewController.h"

@interface AGGroupNameViewController () <UITextFieldDelegate>


@end

@implementation AGGroupNameViewController

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem.enabled = NO;
    [self prepareDataForTable];
}

#pragma mark - Table view methods

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AGSectionType sectionType = [self whichSection:indexPath.section];

    if (sectionType == AGSectionTypeNameStatic) {
        AGGroupNameTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AGGroupNameTableViewCell"];
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"AGGroupNameTableViewCell" owner:nil options:nil];
            cell = [topLevelObjects objectAtIndex:0];
            cell.groupName.delegate = self;
        }
        
        cell.groupName.text = self.userTypedGroupName;
        return cell;
    }
    
    AGAddGroupContactCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AGAddGroupContactCell"];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"AGAddGroupContactCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    AGWOTUser *friend = self.tableData[ sectionType == AGSectionTypeFriendsIngame ? kAGUsersIngame : kAGUsersNotIngame][indexPath.row];
    cell.nickNameLabel.text = friend.nickname;
    [cell setCellSelected:NO];
    cell.watchIcon.hidden = ![self userInWatchingList:friend];
    
    CGRect labelRect = cell.nickNameLabel.frame;
    CGSize maximumLabelSize = labelRect.size;
    NSStringDrawingOptions options = NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin;
    NSDictionary *attr = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Medium" size:17.0]};
    CGRect labelBounds = [cell.nickNameLabel.text boundingRectWithSize:maximumLabelSize
                                                         options:options
                                                      attributes:attr
                                                         context:nil];
    
    cell.onlineIconHorisontalSpacintConstraint.constant = labelRect.origin.x + labelBounds.size.width + 8;
    cell.onlineIcon.hidden = ![self isOnline:friend];

    return cell;
}

- (BOOL) isOnline:(AGWOTUser *) user{
    NSDate * lastTimeOnline = [user.parseObject objectForKey:@"wotOnline"];
    if (!lastTimeOnline) {
        return NO;
    }
    NSTimeInterval distanceBetweenDates = [[NSDate date] timeIntervalSinceDate:lastTimeOnline];
    NSInteger minutesBetweenDates = floor(distanceBetweenDates / 60);
    if (minutesBetweenDates < 30 && minutesBetweenDates > 0) {
        return YES;
    }
    return NO;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger sectionCount = 0;
    if (((NSArray*)self.tableData[kAGUsersIngame]).count) sectionCount++;
    if (((NSArray*)self.tableData[kAGUsersNotIngame]).count) sectionCount++;
    return 1 + sectionCount;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    AGSectionType sectionType = [self whichSection:section];
    if (sectionType == AGSectionTypeNameStatic) return 1;
    if (sectionType == AGSectionTypeFriendsIngame) return ((NSArray*)self.tableData[kAGUsersIngame]).count;
    if (sectionType == AGSectionTypeFriendsNotIngame) return ((NSArray*)self.tableData[kAGUsersNotIngame]).count;
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    view.backgroundColor = [UIColor colorWithRed:56.0/255.0 green:55.0/255.0 blue:46.0/255.0 alpha:1.0];
    UIImageView *separatorTop = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"newgroup_separator_top"]];
    separatorTop.frame = CGRectMake(0, 0, 320, 1);
    [view addSubview:separatorTop];
    UIImageView *separatorBottom = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"newgroup_separator_bottom"]];
    separatorBottom.frame = CGRectMake(0, 29, 320, 1);
    [view addSubview:separatorBottom];
    if (section == 2) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
        label.text = NSLocalizedString(@"not using app", nil);
        label.textAlignment = NSTextAlignmentCenter;
        label.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
        label.textColor = [UIColor colorWithRed:146.0/255.0 green:146.0/255.0 blue:131.0/255.0 alpha:1];
        [view addSubview:label];
    }
    return view;
}

#pragma mark - IBActions

- (IBAction)createButtonTap:(id)sender {
    if (!self.userTypedGroupName || self.userTypedGroupName.length == 0) return;
    
    NSMutableArray *arrayOfNicknames = [NSMutableArray array];
    for (AGWOTUser *user in self.groupContactsData) {
        [arrayOfNicknames addObject:user.nicknameWithNamespace];
    }
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [AGWarGamingApiWorker addUsers:arrayOfNicknames toGroup:self.userTypedGroupName inList:self.listType forGame:self.gameType];
    [[AGWarGamingApiWorker parse_save] continueWithBlock:^id(BFTask *task) {
        [SVProgressHUD dismiss];
        if (task.error) {
            NSLog(@"%@", task.error);
        }
        
        NSArray *viewControllers = [[self navigationController] viewControllers];
        for( int i=0;i<[viewControllers count];i++){
            id obj=[viewControllers objectAtIndex:i];
            if([obj isKindOfClass:[AGListViewController class]]) {
                [[self navigationController] popToViewController:obj animated:YES];
            }
        }
        return nil;
    }];
}

#pragma mark - UITextField delegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    self.userTypedGroupName = resultString;
    self.navigationItem.rightBarButtonItem.enabled = resultString.length != 0;
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(AGSectionType)whichSection:(NSInteger)section
{
    BOOL userHasFriendsIngame = ((NSArray*)self.tableData[kAGUsersIngame]).count;
    BOOL userHasFriendsNotIngame = ((NSArray*)self.tableData[kAGUsersNotIngame]).count;
    NSMutableArray *sectionsArray = [NSMutableArray array];
    [sectionsArray addObject:@(AGSectionTypeNameStatic)];
    if (userHasFriendsIngame) {
        [sectionsArray addObject:@(AGSectionTypeFriendsIngame)];
    }
    if (userHasFriendsNotIngame) {
        [sectionsArray addObject:@(AGSectionTypeFriendsNotIngame)];
    }
    
    [sectionsArray addObject:@(AGSectionTypeEditActionStatic)];
    [sectionsArray addObject:@(AGSectionTypeDeleteActionStatic)];
    
    NSNumber *result = sectionsArray[section];
    return (AGSectionType)result.integerValue;
}

-(void)prepareDataForTable
{
    NSArray *ingameArray = [self.groupContactsData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"is_ingame = %@", @(YES)]];
    NSArray *notIngameArray = [self.groupContactsData filteredArrayUsingPredicate:
                               [NSPredicate predicateWithBlock:^BOOL(AGWOTUser *evaluatedObject, NSDictionary *bindings) {
        return ![ingameArray containsObject:evaluatedObject];
    }]];
    
    self.tableData = @{kAGUsersIngame:ingameArray?ingameArray:@[], kAGUsersNotIngame:notIngameArray?notIngameArray:@[]};
}

-(BOOL)userInWatchingList:(AGWOTUser*)user  {
    NSArray *watchList = [AGWarGamingApiWorker watchList];
    NSArray *foundUsers = [watchList filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSString *evaluatedObject, NSDictionary *bindings) {
        if ([evaluatedObject isEqualToString:user.nicknameWithNamespace]) {
            return YES;
        }
        
        return NO;
    }]];
    
    return foundUsers.count;
}

@end
