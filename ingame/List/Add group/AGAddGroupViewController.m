//
//  AGAddGroupViewController.m
//  ingame
//
//  Created by Pavel Ivanov on 27/08/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import "AGAddGroupViewController.h"
#import "AGAddGroupContactCell.h"
#import "AGWOTUser.h"
#import "AGGroupNameViewController.h"
#import "AGWarGamingApiWorker.h"

@interface AGAddGroupViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *selectedFriends;

@end

@implementation AGAddGroupViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.selectedFriends = [NSMutableArray array];
    [self prepareDataForTable];
    if (self.groupName) {
        self.selectedFriends = [NSMutableArray arrayWithArray:self.groupContactsData];
    
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"Done") style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonTap:)];
    }

    [self updateNextButton];
}
    
-(void)doneButtonTap:(id)sender
{
    NSMutableArray *userNicknamesArray = [NSMutableArray array];
    for (AGWOTUser *user in self.selectedFriends) {
        [userNicknamesArray addObject:user.nicknameWithNamespace];
    }

    NSMutableArray *userNicknamesToRemove = [NSMutableArray array];
    for (AGWOTUser *user in self.groupContactsData) {
        [userNicknamesToRemove addObject:user.nicknameWithNamespace];
    }

    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [AGWarGamingApiWorker removeUsers:userNicknamesToRemove fromGroup:self.groupName inList:self.listType forGame:self.gameType];
    [[[AGWarGamingApiWorker parse_save] continueWithBlock:^id(BFTask *task) {
        [AGWarGamingApiWorker addUsers:userNicknamesArray toGroup:self.groupName inList:self.listType forGame:self.gameType];
        return [AGWarGamingApiWorker parse_save];
    }] continueWithBlock:^id(BFTask *task) {
        [SVProgressHUD dismiss];
        [self.navigationController popViewControllerAnimated:YES];
        if (self.delegate && [self.delegate respondsToSelector:@selector(addGroupControllerAddingFinished:)]) {
            [self.delegate addGroupControllerAddingFinished:self];
        }
        if (task.error) {
            NSLog(@"%@", task.error);
        }
        return nil;
    }];
}

#pragma mark - Table view methods

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AGSectionType sectionType = [self whichSection:indexPath.section];
    
    AGAddGroupContactCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AGAddGroupContactCell" forIndexPath:indexPath];
    AGWOTUser *friend;
    if (sectionType == AGSectionTypeFriendsIngame) {
        friend = self.tableData[kAGUsersIngame][indexPath.row];
    } else {
        friend = self.tableData[kAGUsersNotIngame][indexPath.row];
    }
    
    cell.nickNameLabel.text = friend.nickname;
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:0.961 green:0.349 blue:0.004 alpha:1.000];
    [cell setSelectedBackgroundView:bgColorView];
    
    [cell setCellSelected:[self.selectedFriends containsObject:friend]];
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) return ((NSArray*)self.tableData[kAGUsersIngame]).count;
    if (section == 1) return ((NSArray*)self.tableData[kAGUsersNotIngame]).count;
    return 0;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section < 1) return 0;
    return 30;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section < 1) return nil;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    view.backgroundColor = [UIColor colorWithRed:56.0/255.0 green:55.0/255.0 blue:46.0/255.0 alpha:1.0];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    label.text = NSLocalizedString(@"not using app", nil);
    label.textAlignment = NSTextAlignmentCenter;
    label.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
    label.textColor = [UIColor colorWithRed:146.0/255.0 green:146.0/255.0 blue:131.0/255.0 alpha:1];
    [view addSubview:label];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AGSectionType sectionType = [self whichSection:indexPath.section];
    AGAddGroupContactCell *selectedCell = (AGAddGroupContactCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    AGWOTUser *friend;
    if (sectionType == AGSectionTypeFriendsIngame) {
        friend = self.tableData[kAGUsersIngame][indexPath.row];
    } else {
        friend = self.tableData[kAGUsersNotIngame][indexPath.row];
    }
    
    if ([self.selectedFriends containsObject:friend]) {
        [self.selectedFriends removeObject:friend];
        [selectedCell setCellSelected:NO];
    } else {
        [self.selectedFriends addObject:friend];
        [selectedCell setCellSelected:YES];
    }
    
    [self updateNextButton];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Group Name"]) {
        AGGroupNameViewController *groupNameViewController = segue.destinationViewController;
        groupNameViewController.groupContactsData = self.selectedFriends;
        groupNameViewController.gameType = self.gameType;
        groupNameViewController.listType = self.listType;
    }
}

-(AGSectionType)whichSection:(NSInteger)section
{
    BOOL userHasFriendsIngame = ((NSArray*)self.tableData[kAGUsersIngame]).count;
    BOOL userHasFriendsNotIngame = ((NSArray*)self.tableData[kAGUsersNotIngame]).count;
    NSMutableArray *sectionsArray = [NSMutableArray array];
    if (userHasFriendsIngame) {
        [sectionsArray addObject:@(AGSectionTypeFriendsIngame)];
    }
    if (userHasFriendsNotIngame) {
        [sectionsArray addObject:@(AGSectionTypeFriendsNotIngame)];
    }
    
    [sectionsArray addObject:@(AGSectionTypeEditActionStatic)];
    [sectionsArray addObject:@(AGSectionTypeDeleteActionStatic)];
    
    NSNumber *result = sectionsArray[section];
    return (AGSectionType)result.integerValue;
}

-(void)prepareDataForTable
{
    NSArray *ingameArray = [self.allContactsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"is_ingame = %@", @(YES)]];
    NSArray *notIngameArray = [self.allContactsArray filteredArrayUsingPredicate:
                               [NSPredicate predicateWithBlock:^BOOL(AGWOTUser *evaluatedObject, NSDictionary *bindings) {
        return ![ingameArray containsObject:evaluatedObject];
    }]];
    
    self.tableData = @{kAGUsersIngame:ingameArray?ingameArray:@[], kAGUsersNotIngame:notIngameArray?notIngameArray:@[]};
}

-(void)updateNextButton
{
    self.navigationItem.rightBarButtonItem.enabled = self.selectedFriends && self.selectedFriends.count > 0;
}

@end
