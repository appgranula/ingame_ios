//
//  AGAddGroupContactCell.m
//  ingame
//
//  Created by Pavel Ivanov on 27/08/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import "AGAddGroupContactCell.h"

@implementation AGAddGroupContactCell

@synthesize isCellSelected = _isCellSelected;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

-(void)setCellSelected:(BOOL)isSelected
{
    self.markImageView.hidden = !isSelected;
    _isCellSelected = isSelected;
}

-(void)willTransitionToState:(UITableViewCellStateMask)state
{
    [super willTransitionToState:state];
    if (state != UITableViewCellStateDefaultMask) {
        [UIView animateWithDuration:0.33 animations:^{
            self.watchIcon.alpha = 0;
        }];
    } else {
        [UIView animateWithDuration:0.33 animations:^{
            self.watchIcon.alpha = 1;
        }];
    }
}

@end
