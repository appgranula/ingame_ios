//
//  AGAddGroupViewController.h
//  ingame
//
//  Created by Pavel Ivanov on 27/08/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AGGroupInfoViewController.h"
#import "AGWarGamingApiWorker.h"

@class AGAddGroupViewController;

@protocol AGAddGroupViewcontrollerProtocol <NSObject>

-(void)addGroupControllerAddingFinished:(AGAddGroupViewController*)controller;

@end

@interface AGAddGroupViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSDictionary *tableData;

@property (strong, nonatomic) NSArray *allContactsArray;

@property (strong, nonatomic) NSArray *groupContactsData;
@property (strong, nonatomic) NSString *groupName;
@property (strong, nonatomic) id<AGAddGroupViewcontrollerProtocol> delegate;
@property (strong, nonatomic) AGGameType *gameType;
@property (assign, nonatomic) AGListType listType;

@end
