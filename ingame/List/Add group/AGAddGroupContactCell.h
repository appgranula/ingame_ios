//
//  AGAddGroupContactCell.h
//  ingame
//
//  Created by Pavel Ivanov on 27/08/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGAddGroupContactCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *markImageView;
@property (assign, nonatomic, readonly) BOOL isCellSelected;
@property (strong, nonatomic) IBOutlet UIImageView *watchIcon;

-(void)setCellSelected:(BOOL)isSelected;
@property (strong, nonatomic) IBOutlet UIImageView *onlineIcon;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *onlineIconHorisontalSpacintConstraint;

@end
