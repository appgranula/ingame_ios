//
//  AGFriendListCell.m
//  ingame
//
//  Created by Pavel Ivanov on 15/08/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import "AGFriendListCell.h"

@implementation AGFriendListCell

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [self highlightCell:highlighted];
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [self highlightCell:selected];
}

-(void)setSelected:(BOOL)selected
{
    [self highlightCell:selected];
}

-(void)highlightCell:(BOOL)isOn
{
    if (isOn) {
        self.contentView.backgroundColor = [UIColor colorWithRed:0.573 green:0.573 blue:0.514 alpha:1.000];
    } else {
        self.contentView.backgroundColor = [UIColor clearColor];
    }

}

@end
