//
//  AGListViewController.m
//  ingame
//
//  Created by Pavel Ivanov on 14/08/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import "AGListViewController.h"
#import "AGWarGamingApiWorker.h"
#import "AGWOTUser.h"
#import "AGFriendListCell.h"
#import <SVProgressHUD.h>
#import "AGAddGroupViewController.h"
#import "AGGroupCell.h"
#import <Underscore.h>
#import "AGAboutViewController.h"
#import "AGGroupInfoViewController.h"
#import <MGSwipeButton.h>
#import <iOS-Slide-Menu/SlideNavigationController.h>
#import "UIAlertView+Blocks.h"
#import "UIColor+AGColor.h"

NSString* const kAGUsersIngame = @"kAGUsersIngame";
NSString* const kAGUsersNotIngame = @"kAGUsersNotIngame";
NSString* const kAGUsersGroups = @"kAGUsersGroups";

@interface AGListViewController () <UITableViewDelegate, UISearchBarDelegate, MGSwipeTableCellDelegate, SlideNavigationControllerDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) NSMutableArray *friendsList;
@property (nonatomic, strong) NSMutableDictionary *tableData;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) NSMutableDictionary *pushTasksDictionary;
@property (nonatomic, strong) NSDictionary *groupInfoShowingFor;

@end

@implementation AGListViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.gameType = kAGGameTypeWorldOfTanks;
    self.pushTasksDictionary = [NSMutableDictionary dictionary];
    self.tableView.tableHeaderView = nil;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.searchBar.delegate = self;
    [self.searchBar setBackgroundImage:[[UIImage alloc]init]];
    [self.searchBar setImage:[UIImage imageNamed:@"glass"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateHighlighted];
    [self.searchBar setImage:[UIImage imageNamed:@"glass"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    
    [self.searchBar setImage:[UIImage imageNamed:@"x"] forSearchBarIcon:UISearchBarIconClear state:UIControlStateHighlighted];
    [self.searchBar setImage:[UIImage imageNamed:@"x"] forSearchBarIcon:UISearchBarIconClear state:UIControlStateNormal];
    
    [self.searchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@"bb"] forState:UIControlStateNormal];
    CGRect newRect = self.headerView.frame;
    newRect.size.height = [[UIScreen mainScreen] bounds].size.height - 64;
    self.headerView.frame = newRect;
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.listType = AGListTypeFriends;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didOpen) name:SlideNavigationControllerDidOpen object:nil];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }

// uncomment for testing watch alert
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:@(NO) forKey:@"dontShowWatchAlert"];
//    [defaults synchronize];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setupNavigationBar];
    [self.searchBar setText:@""];
    [self.headerView layoutIfNeeded];
    [UIView animateWithDuration:0.2f animations:^{
        self.searchBar_trailingConstraint.constant = 0;
        [self.headerView layoutIfNeeded];
    }];
    [SVProgressHUD showWithMaskType:(SVProgressHUDMaskTypeBlack)];
    [self getData];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Add Group"]) {
        AGAddGroupViewController *addGroupVC = segue.destinationViewController;
        addGroupVC.allContactsArray = self.friendsList;
        addGroupVC.gameType = self.gameType;
        addGroupVC.listType = self.listType;
    } else if ([segue.identifier isEqualToString:@"Group Details"]) {
        AGGroupInfoViewController *groupInfoVC = segue.destinationViewController;
        groupInfoVC.group = self.groupInfoShowingFor.allKeys[0];
        groupInfoVC.listType = self.listType;
        groupInfoVC.gameType = self.gameType;
        NSArray *usersNicknames = [self.groupInfoShowingFor allValues][0];
        NSMutableArray *arrayOfUsersInGroup = [NSMutableArray array];
        for (NSString *wotUserNickname in usersNicknames) {
            for (AGWOTUser *user in self.friendsList) {
                if ([[user.parseObject objectForKey:@"wotNickname"] isEqualToString:wotUserNickname] ||
                    [user.nicknameWithNamespace isEqualToString:wotUserNickname]) {
                    [arrayOfUsersInGroup addObject:user];
                }
            }
        }

        groupInfoVC.allFriendsData = self.friendsList;
        groupInfoVC.groupContactsData = arrayOfUsersInGroup;

    }
}

#pragma mark - UITableView Datasource and Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AGTapForSendTableViewCell *cell;

    if (indexPath.section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"AGGroupCell" forIndexPath:indexPath];
        NSDictionary *groupDictionary = [self getGroupByIndex:indexPath.row];
        NSArray *group = [groupDictionary allValues].firstObject;
        NSString *groupName = [groupDictionary allKeys].firstObject;
        NSInteger onlineCounter = 0;
        NSInteger allMembersCounter = 0;
        for (NSString *userWotName in group) {
            BOOL userInFriends = NO;
            for (AGWOTUser *userInFriendList in self.friendsList) {
                if ([userInFriendList.nicknameWithNamespace isEqualToString:userWotName]) {
                    userInFriends = YES;
                    break;
                }
            }

            if (!userInFriends) continue;
            allMembersCounter++;
            for (AGWOTUser *wotUser in self.tableData[kAGUsersIngame]) {
                NSString *wotNickname = [wotUser.parseObject objectForKey:@"wotNickname"];
                if ([wotNickname isEqualToString:userWotName]) {
                    if ([self isOnline:wotUser]) {
                        onlineCounter++;
                    }
                }
            }
        }
        
        ((AGGroupCell*)cell).detailsLabel.text = [NSString localizedStringWithFormat:NSLocalizedString(@"%d of %d members is online", @"%d of %d members is online"), onlineCounter, allMembersCounter];
        ((AGGroupCell*)cell).nameLabel.text = groupName;
        ((AGGroupCell*)cell).iButton.tag = indexPath.row;
        [((AGGroupCell*)cell).iButton addTarget:self action:@selector(showGroupInfo:) forControlEvents:UIControlEventTouchUpInside];
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"AGFriendListCell" forIndexPath:indexPath];
        NSArray *sectionArray = [self.tableData objectForKey: indexPath.section == 1 ? kAGUsersIngame : kAGUsersNotIngame];
        AGWOTUser *friend = sectionArray[indexPath.row];
        ((AGFriendListCell *)cell).nameLabel.text = friend.nickname;
        UIView *selectionColor = [[UIView alloc] init];
        selectionColor.backgroundColor = [UIColor colorWithRed:(245/255.0) green:(245/255.0) blue:(245/255.0) alpha:1];
        cell.selectedBackgroundView = selectionColor;
        ((AGFriendListCell *)cell).ImageView_isOnline.hidden = ![self isOnline:friend];
        
        if (self.pushTasksDictionary[friend.nickname]) {
            BFTask *task = self.pushTasksDictionary[friend.nickname];
            if (!task.isCompleted) {
                [cell startAnimating];
            } else {
                [cell cancelAnimation];
            }
        } else {
            [cell cancelAnimation];
        }

        cell.delegate = self;

        ((AGFriendListCell *)cell).watchIcon.hidden = ![self userInWatchingList:friend];
        
        
        CGRect labelRect = ((AGFriendListCell *)cell).nameLabel.frame;
        NSStringDrawingOptions options = NSStringDrawingTruncatesLastVisibleLine |
        NSStringDrawingUsesLineFragmentOrigin;
        NSDictionary *attr = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Medium" size:17.0]};
        CGRect labelBounds = [((AGFriendListCell *)cell).nameLabel.text boundingRectWithSize:CGSizeMake(320.f, CGFLOAT_MAX)
                                                                                     options:options
                                                                                  attributes:attr
                                                                                     context:nil];
        
        ((AGFriendListCell *)cell).onlineHorisontalSpacingConstaint.constant =  labelRect.origin.x + labelBounds.size.width + 8;
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!self.friendsList || !self.friendsList.count) return 0;
    if (section == 0) return ((NSArray*)self.tableData[kAGUsersGroups]).count;
    if (section == 1) return ((NSArray*)self.tableData[kAGUsersIngame]).count;
    if (section == 2) return ((NSArray*)self.tableData[kAGUsersNotIngame]).count;
    return 0;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (!self.friendsList || !self.friendsList.count) return 0;
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section < 2) return 0;
    return 30;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section < 2) return nil;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    view.backgroundColor = [UIColor colorWithRed:56.0/255.0 green:55.0/255.0 blue:46.0/255.0 alpha:1.0];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    label.text = NSLocalizedString(@"not using app", nil);
    label.textAlignment = NSTextAlignmentCenter;
    label.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
    label.textColor = [UIColor colorWithRed:146.0/255.0 green:146.0/255.0 blue:131.0/255.0 alpha:1];
    [view addSubview:label];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AGTapForSendTableViewCell *selectedCell = (AGFriendListCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    
    if (indexPath.section == 0) {
        [selectedCell setSelected:NO];
        NSDictionary *groupDictionary = [self getGroupByIndex:indexPath.row];
        NSArray *group = [groupDictionary allValues].firstObject;
        NSString *groupName = [groupDictionary allKeys].firstObject;
        
        NSMutableArray *usersInGroups = [NSMutableArray array];
        for (NSString *userWotName in group) {
            for (AGWOTUser *user in self.friendsList) {
                if ([user.nicknameWithNamespace isEqualToString:userWotName]){
                    [usersInGroups addObject:user];
                }
            }
        }
        
        if ([self.pushTasksDictionary objectForKey:groupName]) {
            BFTask *task = [self.pushTasksDictionary objectForKey:groupName];
            if (!task.isCompleted) {
                [selectedCell setSelected:NO];
                return;
            }
        }
        
        for (NSString *userWotName in group) {
            if ([self.pushTasksDictionary objectForKey:userWotName]) {
                BFTask *task = [self.pushTasksDictionary objectForKey:userWotName];
                if (!task.isCompleted) {
                    [selectedCell setSelected:NO];
                    return;
                }
            }
        }
        
        [selectedCell startAnimating];
        BFTask *task = [[AGWarGamingApiWorker sendPushTo:usersInGroups ForGame:self.gameType] continueWithBlock:^id(BFTask *task) {
            AGFriendListCell *selectedCell = (AGFriendListCell*)[self.tableView cellForRowAtIndexPath:indexPath];
            [selectedCell stopAnimatingWithResult:task.error CompleteCallback:^{
                
            }];
            
            return nil;
        }];
        
        [self.pushTasksDictionary setObject:task forKey:groupName];

    } else if (indexPath.section == 1) {
        NSArray *sectionArray = [self.tableData objectForKey:kAGUsersIngame];
        AGWOTUser *friend = sectionArray[indexPath.row];
        if ([self.pushTasksDictionary objectForKey:friend.nickname]) {
            BFTask *task = [self.pushTasksDictionary objectForKey:friend.nickname];
            if (!task.isCompleted) {
                [selectedCell setSelected:NO];
                return;
            }
        }
        
        
        [selectedCell startAnimating];
        BFTask *task = [[AGWarGamingApiWorker sendPushTo:@[friend] ForGame:self.gameType] continueWithBlock:^id(BFTask *task) {
            AGFriendListCell *selectedCell = (AGFriendListCell*)[self.tableView cellForRowAtIndexPath:indexPath];
            [selectedCell stopAnimatingWithResult:task.error CompleteCallback:^{
                
            }];
            
            return nil;
        }];
        
        [self.pushTasksDictionary setObject:task forKey:friend.nickname];
    } else if (indexPath.section == 2){
        NSArray *sectionArray = [self.tableData objectForKey:kAGUsersNotIngame];
        AGWOTUser *friend = sectionArray[indexPath.row];
        NSString *pr = NSLocalizedString(@"%@, install the app World of Tanks Ingame so we can call each other quickly ingame — http://iamingame.com/dl", nil);
        NSString *promoteString = [NSString stringWithFormat:pr, friend.nickname];
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:[NSArray arrayWithObjects:promoteString, nil] applicationActivities:nil];
        activityVC.excludedActivityTypes = @[ UIActivityTypeCopyToPasteboard,
                                              UIActivityTypePostToWeibo,
                                              UIActivityTypeSaveToCameraRoll,
                                              UIActivityTypeCopyToPasteboard,
                                              UIActivityTypePrint];
        
        [activityVC setCompletionHandler:^(NSString *activityType, BOOL completed) {
            [self dismissViewControllerAnimated:YES completion:nil];
            AGFriendListCell *selectedCell = (AGFriendListCell*)[self.tableView cellForRowAtIndexPath:indexPath];
            [selectedCell setHighlighted:NO];
            [selectedCell setSelected:NO];
        }];
        
        [self presentViewController:activityVC animated:YES completion:nil];
    }
}

#pragma mark - UISearchBar Delegate

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
     [self.headerView layoutIfNeeded];
     [UIView animateWithDuration:0.2f animations:^{
         self.searchBar_trailingConstraint.constant = 54;
         [self.headerView layoutIfNeeded];
     }];
    return true;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [self refreshBySearch:searchText];
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    if (searchBar.text.length == 0) {
        [self.headerView layoutIfNeeded];
        [UIView animateWithDuration:0.2f animations:^{
            self.searchBar_trailingConstraint.constant = 0;
            [self.headerView layoutIfNeeded];
        }];
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.searchBar resignFirstResponder];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.searchBar resignFirstResponder];
}

#pragma mark - Slide Menu Delegate

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
    return NO;
}

#pragma mark - MGSwipeTableDelegate

-(BOOL)swipeTableCell:(MGSwipeTableCell *)cell canSwipe:(MGSwipeDirection)direction
{
    return !(direction == MGSwipeDirectionLeftToRight);
}

-(NSArray *)swipeTableCell:(MGSwipeTableCell *)cell swipeButtonsForDirection:(MGSwipeDirection)direction swipeSettings:(MGSwipeSettings *)swipeSettings expansionSettings:(MGSwipeExpansionSettings *)expansionSettings
{
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 101, 60)];
    button.backgroundColor = [UIColor colorWithRed:0.231 green:0.231 blue:0.165 alpha:1.000];
    button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    button.titleLabel.numberOfLines = 2;
    [button setTitleColor:[UIColor colorWithRed:0.976 green:0.965 blue:0.867 alpha:1.000] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17.0];
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    button.tag = indexPath.row;
    NSArray *sectionArray = [self.tableData objectForKey: indexPath.section == 1 ? kAGUsersIngame : kAGUsersNotIngame];
    AGWOTUser *friend = sectionArray[indexPath.row];
    [button addTarget:self action:@selector(watchButtonTouch:) forControlEvents:UIControlEventTouchUpInside];
    cell.rightButtons = @[button];
    cell.rightSwipeSettings.transition = MGSwipeTransitionStatic;
    
    if ([self userInWatchingList:friend]) {
        [button setTitle:NSLocalizedString(@"Stop watching", @"Stop watching") forState:UIControlStateNormal];
    } else {
        [button setTitle:NSLocalizedString(@"Start watching", @"Start watching") forState:UIControlStateNormal];
    }
    return @[button];
}

#pragma mark - IBActions

-(void)refreshBySearch: (NSString *) searchText{
    if (!searchText || searchText.length == 0) {
        [self prepareTableDataAndReload];
        return;
    }
    
    self.friendsList = [NSMutableArray arrayWithArray:[self.friendsList sortedArrayUsingSelector:@selector(compare:)]];
    NSArray *friends = [[NSArray alloc] initWithArray:self.friendsList];
    if (searchText.length != 0) {
        friends = Underscore.array(self.friendsList).filter(^BOOL (AGWOTUser *wotUser) {
            return [wotUser.nickname  rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound ;
        }).unwrap;
    }
    
    NSArray *ingameArray =  Underscore.array(friends).filter(^BOOL (AGWOTUser *wotUser) {
        return wotUser.is_ingame;
    }).unwrap;
    
    NSArray *notIngameArray = Underscore.array(friends).without(ingameArray).unwrap;
    
    
    NSDictionary *groups = self.tableData[kAGUsersGroups];
    NSArray *keysArray = [groups allKeys];
    NSArray *filteredArray = Underscore.array(keysArray).filter(^BOOL (NSString *groupName) {
        return [groupName rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound ;
    }).unwrap;
    NSDictionary *filteredGroups = [NSDictionary dictionaryWithObjects:[groups objectsForKeys:filteredArray notFoundMarker:@""] forKeys:filteredArray];
    
    
    self.tableData[kAGUsersGroups] = filteredGroups;
    self.tableData[kAGUsersIngame] = ingameArray?ingameArray:@[];
    self.tableData[kAGUsersNotIngame] = notIngameArray?notIngameArray:@[];
    [self.tableView reloadData];
}

- (IBAction)cancel_searchBarTap:(id)sender {
    [self resignSearchBar];
}

#pragma mark - Handlers

-(void)didOpen
{
    [self.searchBar setText:@""];
    [self.searchBar resignFirstResponder];
    [self.headerView layoutIfNeeded];
    [UIView animateWithDuration:0.2f animations:^{
        self.searchBar_trailingConstraint.constant = 0;
        [self.headerView layoutIfNeeded];
    }];
}

-(void)watchButtonTouch:(UIButton*)sender
{
    CGPoint buttonOriginInTableView = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonOriginInTableView];
    AGSectionType sectionType = [self whichSection:indexPath.section];
    AGWOTUser *user;
    if (sectionType == AGSectionTypeFriendsIngame) {
        user = ((NSArray*)self.tableData[kAGUsersIngame])[indexPath.row];
    } else {
        user = ((NSArray*)self.tableData[kAGUsersNotIngame])[indexPath.row];
    }
    
    if ([self userInWatchingList:user]) {
        [AGWarGamingApiWorker unwatchUsers:@[user.nicknameWithNamespace]];
        [self saveToParseAndReloadCellAtIndexPath:indexPath];
    } else {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber *dontShowWatchAlert = [defaults objectForKey:@"dontShowWatchAlert"];
        if (!dontShowWatchAlert.boolValue) {
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Start watching", @"Start watching")
                                                         message:[NSString localizedStringWithFormat:NSLocalizedString(@"Notify me, when %@ launches game.\n\nWarning! %@ have to install “Ingame Online” mod to make it working.", @"Notify me, when %@ launches game.\n\nWarning! %@ have to install “Ingame Online” mod to make it working."), user.nickname, user.nickname]
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                               otherButtonTitles:NSLocalizedString(@"Ok, don't show again", @"Ok, don't show again"),   NSLocalizedString(@"Ok", @"Ok"), nil];
            
//            Оповещать меня, когда %@ входит в игру. \nВнимание! У %@ должен стоять мод “Ingame Online”, чтобы эта функция работала. 
            
            av.tapBlock = ^(UIAlertView *alertView, NSInteger buttonIndex) {
                if (buttonIndex == alertView.firstOtherButtonIndex  ||
                    buttonIndex == alertView.firstOtherButtonIndex + 1) {
                    [AGWarGamingApiWorker watchUsers:@[user.nicknameWithNamespace]];
                    [self saveToParseAndReloadCellAtIndexPath:indexPath];

                    if (buttonIndex == alertView.firstOtherButtonIndex) {
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        [defaults setObject:@(YES) forKey:@"dontShowWatchAlert"];
                        [defaults synchronize];
                    }
                }
                else if (buttonIndex == alertView.cancelButtonIndex) {
                    AGFriendListCell *cell = (AGFriendListCell*)[self.tableView cellForRowAtIndexPath:indexPath];
                    if ([cell isKindOfClass:[AGFriendListCell class]]) {
                        [cell hideSwipeAnimated:YES];
                    }
                }
            };
            
            [av show];
        } else {
            [AGWarGamingApiWorker watchUsers:@[user.nicknameWithNamespace]];
            [self saveToParseAndReloadCellAtIndexPath:indexPath];

        }
    }
}

-(void)showGroupInfo:(id)sender{
    UIButton *button = sender;
    NSDictionary *groupDictionary = [self getGroupByIndex:button.tag];
    self.groupInfoShowingFor = groupDictionary;
    [self performSegueWithIdentifier:@"Group Details" sender:self];
}

-(void)reloadList
{
    [SVProgressHUD showWithMaskType:(SVProgressHUDMaskTypeBlack)];
    [self setupNavigationBar];
    [self getData];
}

#pragma mark - Private Methods
#pragma mark - Data Methods

-(void)prepareTableDataAndReload
{
    self.tableData = [NSMutableDictionary dictionaryWithDictionary:
                      @{kAGUsersGroups: [NSArray array],
                        kAGUsersIngame: [NSArray array],
                        kAGUsersNotIngame: [NSArray array]}];
    if (!self.friendsList.count) {
        [self.tableView reloadData];
        return;
    }
    
    self.friendsList = [NSMutableArray arrayWithArray:[self.friendsList sortedArrayUsingSelector:@selector(compare:)]];
    NSArray *ingameArray = [self.friendsList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"is_ingame = %@", @(YES)]];
    NSArray *notIngameArray = [self.friendsList filteredArrayUsingPredicate:
                               [NSPredicate predicateWithBlock:^BOOL(AGWOTUser *evaluatedObject, NSDictionary *bindings) {
        return ![ingameArray containsObject:evaluatedObject];
    }]];
    
    NSDictionary *groups = [AGWarGamingApiWorker groupsForGame:self.gameType inList:self.listType];
    self.tableData[kAGUsersGroups] = groups;
    self.tableData[kAGUsersIngame] = ingameArray?ingameArray:@[];
    self.tableData[kAGUsersNotIngame] = notIngameArray?notIngameArray:@[];
    
    [self.tableView reloadData];
}

-(void)getData
{
    [[[AGWarGamingApiWorker parse_refresh] continueWithBlock:^id(BFTask *task) {
        if (self.listType == AGListTypeFriends) {
            return [AGWarGamingApiWorker fetchAllFriendsForGame:self.gameType];
        } else {
            return [AGWarGamingApiWorker fetchAllClanMembers];
        }
    }] continueWithBlock:^id(BFTask *task) {
        [SVProgressHUD dismiss];
        if (task.error) {
            self.tableView.tableHeaderView = self.headerView;
            [self showTableHeaderWithMessage:[MRErrorFormatter stringFromError:task.error]];
            return nil;
        } else {
            
            self.friendsList = task.result;
            for (AGWOTUser *user in self.friendsList) {
                if ([user.nicknameWithNamespace isEqualToString:[AGWarGamingApiWorker wotUser].nicknameWithNamespace]) {
                    [self.friendsList removeObject:user];
                    break;
                }
            }
            
            if (self.friendsList.count == 0) {
                NSString *message;
                if ([self.gameType isEqualToString:kAGGameTypeWorldOfTanks]) {
                    message = NSLocalizedString(@"You don’t have a friends. Find them in World Of Tanks.", nil);
                }
                if ([self.gameType isEqualToString:kAGGameTypeWorldOfWarplanes]) {
                    message = NSLocalizedString(@"You don’t have a friends. Find them in World Of Warplanes.", nil);
                }
                [self showTableHeaderWithMessage:message];
            } else {
                [self showSearchBarInHeaderView];
            }
            
            if (self.friendsList && self.friendsList.count) {
                self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
            }
            
            [self prepareTableDataAndReload];
            
            [self.refreshControl endRefreshing];
        }
        return [[AGWarGamingApiWorker wot_fetchClanInfo] continueWithBlock:^id(BFTask *task) {
            id clanInfo;
            @try {
                clanInfo = [task.result valueForKey:@"data"];
            }
            @catch (NSException *exception) {
                clanInfo = nil;
            }
            
            [AGWarGamingApiWorker wotUser].clanInfo = clanInfo;
            return nil;
        }];
    }];
}

-(NSDictionary *)getGroupByIndex:(NSInteger)index
{
    NSDictionary *groups = self.tableData[kAGUsersGroups];
    NSArray *filteredArray = [[groups allKeys] sortedArrayUsingSelector:@selector(compare:)];
    NSString *keyOfGroup = [filteredArray objectAtIndex:index];
    NSArray *group = groups[keyOfGroup];
    
    return @{keyOfGroup : group};
}

#pragma mark Table Header And Search Bar Methods

-(void)showTableHeaderWithMessage:(NSString*)message
{
    self.headerViewinfoLabel.hidden = NO;
    self.searchBar.hidden = YES;
    self.button_cancel.hidden = YES;
    self.headerViewinfoLabel.text = message;
    CGRect newRect = self.headerView.frame;
    newRect.size.height = self.tableView.frame.size.height;
    self.headerView.frame = newRect;
    self.tableView.tableHeaderView = self.headerView;
    self.friendsList = [NSMutableArray array];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self prepareTableDataAndReload];
}

-(void)showSearchBarInHeaderView{
    self.headerViewinfoLabel.hidden = YES;
    self.searchBar.hidden = NO;
    self.button_cancel.hidden = NO;
    CGRect newRect = self.headerView.frame;
    newRect.size.height = 44;
    self.headerView.frame = newRect;
    self.tableView.tableHeaderView = self.headerView;
}

- (void) resignSearchBar{
    [self.searchBar setText:@""];
    [self refreshBySearch:@""];
    [self.searchBar resignFirstResponder];
    [self.headerView layoutIfNeeded];
    [UIView animateWithDuration:0.2f animations:^{
        self.searchBar_trailingConstraint.constant = 0;
        [self.headerView layoutIfNeeded];
    }];
}

#pragma mark Watching methods

-(BOOL)userInWatchingList:(AGWOTUser*)user  {
    NSArray *watchList = [AGWarGamingApiWorker watchList];
    NSArray *foundUsers = [watchList filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSString *evaluatedObject, NSDictionary *bindings) {
        if ([evaluatedObject isEqualToString:user.nicknameWithNamespace]) {
            return YES;
        }
        
        return NO;
    }]];
    
    return foundUsers.count;
}

-(void)saveToParseAndReloadCellAtIndexPath:(NSIndexPath*)indexPath
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [[AGWarGamingApiWorker parse_save] continueWithBlock:^id(BFTask *task) {
        [SVProgressHUD dismiss];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        });
        //            [cell setSwipeOffset:0 animated:YES completion:^{
        //                [cell hideSwipeAnimated:NO];
        //                NSLog(@"%@", ipfortest);
        //            }];
        
        if (task.error) {
            NSLog(@"%@", task.error.description);
        }
        return nil;
    }];
}

#pragma mark Other methods

-(void)setupNavigationBar
{
    if (self.listType == AGListTypeFriends) {
        self.topTitle.text = NSLocalizedString(@"Friends", @"Friends");
    } else {
        self.topTitle.attributedText = [self getClanTitle];
    }
    
    if ([self.gameType isEqualToString:kAGGameTypeWorldOfTanks]) {
        self.bottomTitle.text = NSLocalizedString(@"WORLD OF TANKS", @"WORLD OF TANKS");
    } else {
        self.bottomTitle.text = NSLocalizedString(@"WORLD OF WARPLANES", @"WORLD OF WARPLANES");
    }
    
    self.navigationItem.hidesBackButton = NO;
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

-(NSAttributedString*)getClanTitle
{
    NSString *clanColorCode = [[AGWarGamingApiWorker wotUser].clanInfo allValues][0][@"clan_color"];
    UIColor *clanColor = [UIColor getUIColorObjectFromHexString:clanColorCode alpha:1];
    NSString *abbreviation = [[AGWarGamingApiWorker wotUser].clanInfo allValues][0][@"abbreviation"];
    NSString *name = [[AGWarGamingApiWorker wotUser].clanInfo allValues][0][@"name"];
    NSString *clanTitle = [NSString stringWithFormat:@"[%@] %@", abbreviation, name];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:clanTitle];
    [str addAttribute:NSForegroundColorAttributeName value:clanColor range:NSMakeRange(0,1)];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(1,abbreviation.length)];
    [str addAttribute:NSForegroundColorAttributeName value:clanColor range:NSMakeRange(abbreviation.length+1,1)];
    return str;
}

- (BOOL) isOnline:(AGWOTUser *) user{
    NSDate * lastTimeOnline = [user.parseObject objectForKey:@"wotOnline"];
    if (!lastTimeOnline) {
        return NO;
    }
    NSTimeInterval distanceBetweenDates = [[NSDate date] timeIntervalSinceDate:lastTimeOnline];
    NSInteger minutesBetweenDates = floor(distanceBetweenDates / 60.0);
    if (minutesBetweenDates < 30 && minutesBetweenDates >= 0) {
        return YES;
    }
    return NO;
}

-(AGSectionType)whichSection:(NSInteger)section
{
    BOOL userHasFriendsIngame = ((NSArray*)self.tableData[kAGUsersIngame]).count;
    BOOL userHasFriendsNotIngame = ((NSArray*)self.tableData[kAGUsersNotIngame]).count;
    NSMutableArray *sectionsArray = [NSMutableArray array];
    [sectionsArray addObject:@(AGSectionTypeNameStatic)];
    if (userHasFriendsIngame) {
        [sectionsArray addObject:@(AGSectionTypeFriendsIngame)];
    }
    if (userHasFriendsNotIngame) {
        [sectionsArray addObject:@(AGSectionTypeFriendsNotIngame)];
    }
    
    [sectionsArray addObject:@(AGSectionTypeEditActionStatic)];
    [sectionsArray addObject:@(AGSectionTypeDeleteActionStatic)];
    
    NSNumber *result = sectionsArray[section];
    return (AGSectionType)result.integerValue;
}

@end
