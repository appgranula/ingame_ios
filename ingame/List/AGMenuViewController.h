//
//  AGMenuViewController.h
//  ingame
//
//  Created by Pavel Ivanov on 09/09/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGMenuViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;
@end
