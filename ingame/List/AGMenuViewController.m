//
//  AGMenuViewController.m
//  ingame
//
//  Created by Pavel Ivanov on 09/09/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import "AGMenuViewController.h"
#import "AGWarGamingApiWorker.h"
#import <SlideNavigationController.h>
#import "AGListViewController.h"
#import "AGMenuCell.h"
#import <UIImageView+AFNetworking.h>
#import "UIColor+AGColor.h"

@interface AGMenuViewController () <UITableViewDelegate, UITableViewDataSource>

@end

@implementation AGMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
// TODO: передалать под событие на логин
    [[NSNotificationCenter defaultCenter] addObserverForName:SlideNavigationControllerDidOpen object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self.tableView reloadData];
        self.nameLabel.text = [AGWarGamingApiWorker wotUser].nickname;
    }];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.tableView reloadData];
    self.nameLabel.text = [AGWarGamingApiWorker wotUser].nickname;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

#pragma mark - UITableView Delegate & Datasrouce

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        if ([AGWarGamingApiWorker wotUser].clanId && [AGWarGamingApiWorker wotUser].clanInfo) {
            return 2;
        }
        
        return 1;
    } else if (section == 1) {
        return 1;
    } else {
        return 4;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    view.backgroundColor = [UIColor colorWithRed:56.0/255.0 green:55.0/255.0 blue:46.0/255.0 alpha:1.0];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 270, 30)];
    if (section == 0) {
        label.text = NSLocalizedString(@"WORLD OF TANKS", @"WORLD OF TANKS");
    } else if (section == 1) {
        label.text = NSLocalizedString(@"WORLD OF PLANES", @"WORLD OF PLANES");
    } else {
        label.text = NSLocalizedString(@"GENERAL", @"GENERAL");
    }
    
    label.textAlignment = NSTextAlignmentCenter;
    label.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
    label.textColor = [UIColor colorWithRed:146.0/255.0 green:146.0/255.0 blue:131.0/255.0 alpha:1];
    [view addSubview:label];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 30;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	AGMenuCell*cell = [tableView dequeueReusableCellWithIdentifier:@"leftMenuCell" forIndexPath:indexPath];
	if (indexPath.section == 0) {
        switch (indexPath.row)
        {
            case 0:
                cell.titleLabel.text = NSLocalizedString(@"Friends", @"Friends");
                cell.iconImage.image = [UIImage imageNamed:@"menu_icon_friends"];
                break;
       
            case 1: {
                NSString *clanColorCode = [[AGWarGamingApiWorker wotUser].clanInfo allValues][0][@"clan_color"];
                UIColor *clanColor = [UIColor getUIColorObjectFromHexString:clanColorCode alpha:1];
                NSString *abbreviation = [[AGWarGamingApiWorker wotUser].clanInfo allValues][0][@"abbreviation"];
                NSString *name = [[AGWarGamingApiWorker wotUser].clanInfo allValues][0][@"name"];
                NSString *clanTitle = [NSString stringWithFormat:@"[%@] %@", abbreviation, name];
                NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:clanTitle];
                [str addAttribute:NSForegroundColorAttributeName value:clanColor range:NSMakeRange(0,1)];
                [str addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(1,abbreviation.length)];
                [str addAttribute:NSForegroundColorAttributeName value:clanColor range:NSMakeRange(abbreviation.length+1,1)];
                cell.titleLabel.attributedText = str;
                cell.iconImage.image = [UIImage imageNamed:@"menu_icon_clan"];
                if ([[AGWarGamingApiWorker wotUser].clanInfo allValues][0][@"emblems"]) {
                    NSURL *url = [NSURL URLWithString:[[AGWarGamingApiWorker wotUser].clanInfo allValues][0][@"emblems"][@"large"]];
                    [cell.iconImage setImageWithURL:url];
                }

            }
                break;
        }
    }
    
    if (indexPath.section == 1) {
        switch (indexPath.row)
        {
            case 0:
                cell.titleLabel.text = NSLocalizedString(@"Friends", @"Friends");
                cell.iconImage.image = [UIImage imageNamed:@"menu_icon_friends"];
                break;
                
            case 1: {
                
                // TODO: Serious refactoring!!!!111
                NSString *clanColorCode = [[AGWarGamingApiWorker wotUser].clanInfo allValues][0][@"clan_color"];
                UIColor *clanColor = [UIColor getUIColorObjectFromHexString:clanColorCode alpha:1];
                NSString *abbreviation = [[AGWarGamingApiWorker wotUser].clanInfo allValues][0][@"abbreviation"];
                NSString *name = [[AGWarGamingApiWorker wotUser].clanInfo allValues][0][@"name"];
                NSString *clanTitle = [NSString stringWithFormat:@"[%@] %@", abbreviation, name];
                NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:clanTitle];
                [str addAttribute:NSForegroundColorAttributeName value:clanColor range:NSMakeRange(0,1)];
                [str addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(1,abbreviation.length)];
                [str addAttribute:NSForegroundColorAttributeName value:clanColor range:NSMakeRange(abbreviation.length+1,1)];
                cell.titleLabel.attributedText = str;
                cell.iconImage.image = [UIImage imageNamed:@"menu_icon_clan"];
                if ([[AGWarGamingApiWorker wotUser].clanInfo allValues][0][@"emblems"]) {
                    NSURL *url = [NSURL URLWithString:[[AGWarGamingApiWorker wotUser].clanInfo allValues][0][@"emblems"][@"large"]];
                    [cell.iconImage setImageWithURL:url];
                }
                
            }
                break;
        }
    }
    
    if (indexPath.section == 2) {
        switch (indexPath.row)
        {
            case 0:
                cell.titleLabel.text = NSLocalizedString(@"Feedback", @"Feedback");
                cell.iconImage.image = [UIImage imageNamed:@"menu_icon_feedback"];
                break;
            case 1:
                cell.titleLabel.text = NSLocalizedString(@"Rate us", @"Rate us");
                cell.iconImage.image = [UIImage imageNamed:@"menu_icon_rate"];
                break;
            case 2:
                cell.titleLabel.text = NSLocalizedString(@"About us", @"About us");
                cell.iconImage.image = [UIImage imageNamed:@"menu_icon_about"];
                break;
            case 3:
                cell.titleLabel.text = NSLocalizedString(@"Logout", @"Logout");
                cell.iconImage.image = [UIImage imageNamed:@"menu_icon_logout"];
                break;
        }
    }

	return cell;
}

-(AGListViewController *)findList {
    for (UIViewController *vc in [SlideNavigationController sharedInstance].viewControllers) {
        if ([vc isKindOfClass:[AGListViewController class]]) {
            return (AGListViewController*)vc;
        }
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AGMenuCell *selectedCell = (AGMenuCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    [selectedCell setSelected:NO];

	UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
															 bundle: nil];
    NSURL* itunseUrl = [NSURL URLWithString:[@"itms-apps://itunes.apple.com/app/id" stringByAppendingString:ITUNES_ID]];
    NSURL* feedbackUrl = [NSURL URLWithString:[@"mailto:afanasyev@appgranula.com?subject=Ingame feedback&body=" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            AGListViewController *listVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AGListViewController"];
            SlideNavigationController *nc = [SlideNavigationController sharedInstance];
            [nc popToRootAndSwitchToViewController:listVC withCompletion:^{
                AGListViewController *listControllerView = [self findList];
                listControllerView.gameType = kAGGameTypeWorldOfTanks;
                listControllerView.listType = AGListTypeFriends;
                [listControllerView reloadList];
            }];
        }
        if (indexPath.row == 1) {
            AGListViewController *listVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AGListViewController"];
            SlideNavigationController *nc = [SlideNavigationController sharedInstance];
            [nc popToRootAndSwitchToViewController:listVC withCompletion:^{
                AGListViewController *listControllerView = [self findList];
                listControllerView.gameType = kAGGameTypeWorldOfTanks;
                listControllerView.listType = AGListTypeClan;
                [listControllerView reloadList];
            }];
        }
    }
    
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            AGListViewController *listVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AGListViewController"];
            SlideNavigationController *nc = [SlideNavigationController sharedInstance];
            [nc popToRootAndSwitchToViewController:listVC withCompletion:^{
                AGListViewController *listControllerView = [self findList];
                listControllerView.listType = AGListTypeFriends;
                listControllerView.gameType = kAGGameTypeWorldOfWarplanes;
                [listControllerView reloadList];
            }];
        }
    }
    
    if (indexPath.section == 2) {
        switch (indexPath.row)
        {
            case 0: {
                //feedback
                SlideNavigationController *nc = [SlideNavigationController sharedInstance];
                [nc closeMenuWithCompletion:^{
                    [[UIApplication sharedApplication]  openURL:feedbackUrl];
                }];
            }
                break;
            case 1: {
                //rate
                SlideNavigationController *nc = [SlideNavigationController sharedInstance];
                [nc closeMenuWithCompletion:^{
                    [[UIApplication sharedApplication] openURL:itunseUrl];
                }];
            }
                break;
            case 2: {
                UIViewController *aboutVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AGAboutViewController"];
                SlideNavigationController *nc = [SlideNavigationController sharedInstance];
                [nc popToRootAndSwitchToViewController:aboutVC withCompletion:nil];
                
            }
                break;
            case 3: {
                [[AGWarGamingApiWorker logout] continueWithBlock:^id(BFTask *task) {
                    if (task.error) {
                        UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:indexPath];
                        [cell presentError:task.error];
                    }
                    return task;
                }];
            }
                break;
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 43;
}



@end
