//
//  AGTapForSendTableViewCell.h
//  ingame
//
//  Created by Pavel Ivanov on 29/08/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MGSwipeTableCell.h>

@interface AGTapForSendTableViewCell : MGSwipeTableCell

@property (nonatomic, strong) IBOutlet UIView *mainContentView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (strong, nonatomic) IBOutlet UILabel *infoLabel;

-(void)startAnimating;
-(void)stopAnimatingWithResult:(NSError *)error CompleteCallback:(void (^)())callback;
-(void)cancelAnimation;

@end
