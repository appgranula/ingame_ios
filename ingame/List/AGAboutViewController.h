//
//  AGAboutViewController.h
//  ingame
//
//  Created by Андрей Лазарев on 26.08.14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGAboutViewController : UIViewController<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
