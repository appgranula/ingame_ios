//
//  AGAboutViewController.m
//  ingame
//
//  Created by Андрей Лазарев on 26.08.14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import "AGAboutViewController.h"

@interface AGAboutViewController ()
@property BOOL didPassAboutBlank;
@end

@implementation AGAboutViewController

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if ([request.URL.scheme isEqualToString:@"file"]) {
        return YES;
    }
    [[UIApplication sharedApplication] openURL:request.URL];
    return NO;
}
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"setVersion(\"%@\")", version]];
    if ([[webView stringByEvaluatingJavaScriptFromString:@"document.readyState"] isEqualToString:@"complete"]) {
        [UIView animateWithDuration:0.3 delay:0 options:0 animations:^{
            self.webView.alpha = 1;
        } completion:nil];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"About", nil);
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"about" ofType:@"html"];
    NSURL* url = [NSURL fileURLWithPath:filePath isDirectory:NO];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
    self.webView.alpha = 0;
    self.navigationItem.hidesBackButton = NO;
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark - Slide Menu Delegate

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

@end
