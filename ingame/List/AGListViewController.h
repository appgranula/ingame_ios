//
//  AGListViewController.h
//  ingame
//
//  Created by Pavel Ivanov on 14/08/14.
//  Copyright (c) 2014 Eugene. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AGWarGamingApiWorker.h"

extern NSString* const kAGUsersIngame;
extern NSString* const kAGUsersNotIngame;
extern NSString* const kAGUsersGroups;

@interface AGListViewController : UIViewController
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UIView *titleView;
@property (strong, nonatomic) IBOutlet UILabel *topTitle;
@property (strong, nonatomic) IBOutlet UILabel *bottomTitle;
- (IBAction)cancel_searchBarTap:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *headerViewinfoLabel;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (assign, nonatomic) AGListType listType;
@property (strong, nonatomic) AGGameType *gameType;
@property (strong, nonatomic) IBOutlet UIButton *button_cancel;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *searchBar_trailingConstraint;

-(void)reloadList;

@end
