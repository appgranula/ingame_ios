//
//  AGGroupInfoActionCell.h
//  ingame
//
//  Created by Pavel Ivanov on 01/09/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGGroupInfoActionCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *labelButton;

@end
