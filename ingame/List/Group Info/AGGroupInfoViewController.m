//
//  AGVGroupInfoViewController.m
//  ingame
//
//  Created by Pavel Ivanov on 29/08/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import "AGGroupInfoViewController.h"
#import "AGGroupInfoActionCell.h"
#import <SVProgressHUD.h>
#import "AGAddGroupViewController.h"

@interface AGGroupInfoViewController () <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate, AGAddGroupViewcontrollerProtocol>

@end

@implementation AGGroupInfoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.userTypedGroupName = self.group;
    self.navigationItem.rightBarButtonItem.enabled = YES;
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.isMovingFromParentViewController) {
        NSString *groupName = self.group;

        if (![self.userTypedGroupName isEqualToString:groupName]) {
            //group name was changed
            
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
            [AGWarGamingApiWorker removeGroup:groupName inList:self.listType forGame:self.gameType];
            [[[AGWarGamingApiWorker parse_save] continueWithBlock:^id(BFTask *task) {
                NSMutableArray *userNicknamesArray = [NSMutableArray array];
                for (AGWOTUser *user in [self friendsIdsArray]) {
                    [userNicknamesArray addObject:user.nicknameWithNamespace];
                }

                [AGWarGamingApiWorker addUsers:userNicknamesArray toGroup:self.userTypedGroupName inList:self.listType  forGame:self.gameType];
                return [AGWarGamingApiWorker parse_save];
            }] continueWithBlock:^id(BFTask *task) {
                [SVProgressHUD dismiss];
                if (task.error) {
                    NSLog(@"%@", task.error);
                }
                return nil;
            }];
        } else {
            [[AGWarGamingApiWorker parse_save] continueWithBlock:^id(BFTask *task) {
                [SVProgressHUD dismiss];
                if (task.error) {
                    NSLog(@"%@", task.error);
                }
                return nil;
            }];
        }
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Add Users"]) {
        AGAddGroupViewController *addController = segue.destinationViewController;
        addController.allContactsArray = self.allFriendsData;
        addController.groupContactsData = self.groupContactsData;
        addController.groupName = self.group;
        addController.listType = self.listType;
        addController.gameType = self.gameType;
        addController.delegate = self;
    }
}

-(NSArray*)friendsIdsArray
{
    return self.groupContactsData;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AGSectionType sectionType = [self whichSection:indexPath.section];
    switch (sectionType) {
        case AGSectionTypeNameStatic: {
            UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
            ((AGGroupNameTableViewCell*)cell).groupName.text = self.userTypedGroupName;
            return cell;
        }
        case AGSectionTypeFriendsIngame:
            return  [super tableView:tableView cellForRowAtIndexPath:indexPath];
        case AGSectionTypeFriendsNotIngame:
            return  [super tableView:tableView cellForRowAtIndexPath:indexPath];
        case AGSectionTypeEditActionStatic:{
            AGGroupInfoActionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AGGroupInfoActionCell"];
            if (cell == nil) {
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"AGGroupInfoActionCell" owner:self options:nil];
                cell = [topLevelObjects objectAtIndex:0];
                cell.labelButton.text = @"Add members";
            }
            
            return cell;
        }
        case AGSectionTypeDeleteActionStatic: {
            AGGroupInfoActionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AGGroupInfoActionCell"];
            if (cell == nil) {
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"AGGroupInfoActionCell" owner:self options:nil];
                cell = [topLevelObjects objectAtIndex:0];
                cell.labelButton.text = NSLocalizedString(@"Delete group", @"Delete group");
            }
            
            return cell;
        }
        default:
            return 0;
    }
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    AGSectionType sectionType = [self whichSection:indexPath.section];
    if (sectionType == AGSectionTypeFriendsIngame || sectionType == AGSectionTypeFriendsNotIngame) return YES;
    return NO;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger sectionCount = [super numberOfSectionsInTableView:tableView];
    return sectionCount+2;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AGSectionType sectionType = [self whichSection:indexPath.section];
    if (sectionType == AGSectionTypeDeleteActionStatic) {
        [self showAlertOnRemoveWithText:NSLocalizedString(@"Are you sure you want to remove group?", @"Are you sure you want to remove group?")];
    }
    
    if (sectionType == AGSectionTypeEditActionStatic) {
        [self performSegueWithIdentifier:@"Add Users" sender:self];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)showAlertOnRemoveWithText:(NSString*)text
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Delete group", @"Delete group")
                                                        message:text
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                              otherButtonTitles:NSLocalizedString(@"Delete", @"Delete"), nil];
    [alertView show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [self removeGroupAndDismiss];
     }
}

-(void)removeGroupAndDismiss
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [AGWarGamingApiWorker removeGroup:self.group inList:self.listType forGame:self.gameType];
    [[AGWarGamingApiWorker parse_save] continueWithBlock:^id(BFTask *task) {
        [SVProgressHUD dismiss];
        [self.navigationController popViewControllerAnimated:YES];
        if (task.error) {
            NSLog(@"%@", task.error);
        }
        return nil;
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    AGSectionType sectionType = [self whichSection:section];
    switch (sectionType) {
        case AGSectionTypeNameStatic:
            return 1;
        case AGSectionTypeFriendsIngame:
            return ((NSArray*)self.tableData[kAGUsersIngame]).count;
        case AGSectionTypeFriendsNotIngame:
            return ((NSArray*)self.tableData[kAGUsersNotIngame]).count;
        case AGSectionTypeEditActionStatic:
            return 1;
        case AGSectionTypeDeleteActionStatic:
            return 1;
        default:
            return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    AGSectionType sectionType = [self whichSection:section];
    switch (sectionType) {
        case AGSectionTypeFriendsNotIngame:
            return [super tableView:tableView viewForHeaderInSection:2];
        default:
            return [super tableView:tableView viewForHeaderInSection:0];
    }
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSInteger idOfUser = indexPath.row;
        AGSectionType sectionType = [self whichSection:indexPath.section];
        NSString *dataTypeIdentifier;
        if (sectionType == AGSectionTypeFriendsIngame) {
            dataTypeIdentifier = kAGUsersIngame;
        }
        
        if (sectionType == AGSectionTypeFriendsNotIngame) {
            dataTypeIdentifier = kAGUsersNotIngame;
        }
        
        NSString *groupName = self.group;
            NSArray *arrayOfUsersIngame = self.tableData[dataTypeIdentifier];
            AGWOTUser *userToDelete = [arrayOfUsersIngame objectAtIndex:idOfUser];
            [AGWarGamingApiWorker removeUsers:@[userToDelete.nicknameWithNamespace] fromGroup:groupName inList:self.listType forGame:self.gameType];
            NSMutableArray *newArrayWithoutUser = [NSMutableArray array];
            [arrayOfUsersIngame enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                if (obj != userToDelete) {
                    [newArrayWithoutUser addObject:obj];
                }
            }];
            
            NSMutableArray *newFriendsData = [NSMutableArray array];
            for (AGWOTUser *user in self.groupContactsData) {
                if (user != userToDelete) {
                    [newFriendsData addObject:user];
                }
            }
            
            self.groupContactsData = newFriendsData;
            [self prepareDataForTable];
            [tableView reloadData];
    }
}

-(AGSectionType)whichSection:(NSInteger)section
{
    BOOL userHasFriendsIngame = ((NSArray*)self.tableData[kAGUsersIngame]).count;
    BOOL userHasFriendsNotIngame = ((NSArray*)self.tableData[kAGUsersNotIngame]).count;
    NSMutableArray *sectionsArray = [NSMutableArray array];
    [sectionsArray addObject:@(AGSectionTypeNameStatic)];
    if (userHasFriendsIngame) {
        [sectionsArray addObject:@(AGSectionTypeFriendsIngame)];
    }
    if (userHasFriendsNotIngame) {
        [sectionsArray addObject:@(AGSectionTypeFriendsNotIngame)];
    }
    
    [sectionsArray addObject:@(AGSectionTypeEditActionStatic)];
    [sectionsArray addObject:@(AGSectionTypeDeleteActionStatic)];
    
    NSNumber *result = sectionsArray[section];
    return (AGSectionType)result.integerValue;
}

- (IBAction)editButtonTap:(id)sender {
    if (!self.tableView.isEditing) {
        [self.tableView setEditing:YES animated:YES];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"Done")
                                                                                  style:UIBarButtonItemStyleDone
                                                                                 target:self
                                                                                 action:@selector(editButtonTap:)];
    } else {
        
            [self.tableView setEditing:NO animated:YES];
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Edit", @"Edit")
                                                                                      style:UIBarButtonItemStyleDone
                                                                                     target:self
                                                                                     action:@selector(editButtonTap:)];
    }
}

-(void)getData
{
    [[[AGWarGamingApiWorker parse_refresh] continueWithBlock:^id(BFTask *task) {
        return [AGWarGamingApiWorker fetchAllFriendsForGame:self.gameType];
    }] continueWithBlock:^id(BFTask *task) {
        [SVProgressHUD dismiss];
        if (task.error) {
            NSLog(@"%@", task.error);
        } else {
            self.allFriendsData = task.result;
            if (self.allFriendsData && self.allFriendsData.count) {
//                self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
            }
            
            NSMutableArray *usersInGroup = [NSMutableArray array];
            NSDictionary *groups = [AGWarGamingApiWorker groupsForGame:self.gameType inList:self.listType];
            for (NSString *groupName in groups) {
                if ([groupName isEqualToString:self.group]) {
                    usersInGroup = groups[groupName];
                    break;
                }
            }
            
            NSMutableArray *arrayOfUsersInGroup = [NSMutableArray array];
            for (AGWOTUser *user in self.allFriendsData) {
                for (NSString *userId in usersInGroup) {
                    if ([userId isEqualToString:user.nickname] ||
                        [userId isEqualToString:user.nicknameWithNamespace]) {
                        [arrayOfUsersInGroup addObject:user];
                    }
                }
            }

            self.groupContactsData = arrayOfUsersInGroup;
            [self prepareDataForTable];
            [self.tableView reloadData];
        }
        return task;
    }];
}

-(void)addGroupControllerAddingFinished:(AGAddGroupViewController *)controller
{
    [self getData];
}

@end
