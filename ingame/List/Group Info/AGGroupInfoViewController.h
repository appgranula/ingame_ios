//
//  AGVGroupInfoViewController.h
//  ingame
//
//  Created by Pavel Ivanov on 29/08/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AGGroupNameViewController.h"

@interface AGGroupInfoViewController : AGGroupNameViewController
@property (nonatomic, strong) NSString *group;
@property (strong, nonatomic) NSArray *allFriendsData;
@property (strong, nonatomic) AGGameType *gameType;
- (IBAction)editButtonTap:(id)sender;

@end
