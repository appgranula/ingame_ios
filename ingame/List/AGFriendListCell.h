//
//  AGFriendListCell.h
//  ingame
//
//  Created by Pavel Ivanov on 15/08/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AGTapForSendTableViewCell.h"

@interface AGFriendListCell : AGTapForSendTableViewCell

@property (strong, nonatomic) IBOutlet UILabel *nameLabel;

@property (strong, nonatomic) IBOutlet UIImageView *ImageView_isOnline;
@property (strong, nonatomic) IBOutlet UIImageView *watchIcon;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *onlineHorisontalSpacingConstaint;

@end
