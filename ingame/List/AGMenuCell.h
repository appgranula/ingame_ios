//
//  AGMenuCell.h
//  ingame
//
//  Created by Pavel Ivanov on 10/09/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGMenuCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *iconImage;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end
