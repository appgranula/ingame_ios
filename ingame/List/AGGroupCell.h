//
//  AGGroupCell.h
//  ingame
//
//  Created by Pavel Ivanov on 29/08/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AGTapForSendTableViewCell.h"

@interface AGGroupCell : AGTapForSendTableViewCell
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *detailsLabel;
@property (strong, nonatomic) IBOutlet UIButton *iButton;

@end
