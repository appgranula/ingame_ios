//
//  UIColor+AGColor.h
//  ingame
//
//  Created by Pavel Ivanov on 25/09/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (AGColor)
+ (UIColor *)getUIColorObjectFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha;
@end
