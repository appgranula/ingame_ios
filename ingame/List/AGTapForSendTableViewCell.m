//
//  AGTapForSendTableViewCell.m
//  ingame
//
//  Created by Pavel Ivanov on 29/08/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import "AGTapForSendTableViewCell.h"

@implementation AGTapForSendTableViewCell

-(void)startAnimating
{
    [self setSelected:YES];
    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setSelected:NO];
            [self cancelAnimation];
            [self.indicator startAnimating];
            [UIView animateWithDuration:0.35 animations:^{
                self.indicator.alpha = 1;
                self.mainContentView.alpha = 0;
            }];
            
        });
    });
}

-(void)stopAnimatingWithResult:(NSError *)error CompleteCallback:(void (^)())callback
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.indicator.alpha = 1;
        self.infoLabel.alpha = 0;
        
        if (!error) {
            self.infoLabel.text = NSLocalizedString(@"Sent!", nil);
            [UIView animateWithDuration:0.35 animations:^{
                self.indicator.alpha = 0;
                self.infoLabel.alpha = 1;
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.35 delay:1 options:UIViewAnimationOptionCurveLinear animations:^{
                    self.infoLabel.alpha = 0;
                    self.indicator.alpha = 0;
                    self.mainContentView.alpha = 1;
                } completion:^(BOOL finished) {
                    callback();
                }];
            }];
        } else {
            [self presentError:error];
            [UIView animateWithDuration:0.35 animations:^{
                self.indicator.alpha = 0;
                self.mainContentView.alpha = 1;
            } completion:^(BOOL finished) {
                callback();
            }];
        }
    });
}

-(void)cancelAnimation
{
    [self.indicator.layer removeAllAnimations];
    [self.mainContentView.layer removeAllAnimations];
    [self.infoLabel.layer removeAllAnimations];
    self.mainContentView.alpha = 1;
    self.indicator.alpha = 0;
    self.infoLabel.alpha = 0;
}

@end
