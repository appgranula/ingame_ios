//
//  AGAppDelegate.m
//  ingame
//
//  Created by Eugene on 13/08/14.
//  Copyright (c) 2014 Eugene. All rights reserved.
//

#import <TWMessageBarManager.h>

#import "AGAppDelegate.h"
#import "AGWarGamingApiWorker.h"
#import "AGStyleSheetForNotification.h"
#import "AGMenuViewController.h"
#import <SlideNavigationController.h>
#import "NSString+AGParser.h"

@import AudioToolbox;

@interface AGAppDelegate()
@property (assign, nonatomic) SystemSoundID soundID;
@end

@implementation AGAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
#ifdef PRODUCTION
    [Parse setApplicationId:@"JuQVBt8IdYBpRaGLhHkb7tSKon2wukDTbfRw2wYl"
                  clientKey:@"OdDzbpG3g1LIbPBtItSI1QIMUwl1EsgfWMicPsvU"];
#else
    [Parse setApplicationId:@"O0TKlf53X6NewlbqZHNvFOiTNuXZj0jdT6E4y6jk"
                  clientKey:@"En1FHkVrPeA3KkX6OzvE0vnvqsxX3WKe6KHp4Tuc"];
#endif

    [PFAnalytics trackAppOpenedWithLaunchOptions:nil];
    
    [[AGWarGamingApiWorker updateServerInformation] continueWithBlock:^id(BFTask *task) {
        if (task.error) {
            NSLog(@"%@", task.error);
        }
        return nil;
    }];
    
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [application registerForRemoteNotifications];
    } else {
        [application registerForRemoteNotificationTypes:
         UIRemoteNotificationTypeBadge |
         UIRemoteNotificationTypeAlert |
         UIRemoteNotificationTypeSound];
    }
    
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0.231 green:0.231 blue:0.165 alpha:1.000]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:0.976 green:0.965 blue:0.867 alpha:1.000]}];

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    UIColor *mainTextColor = [UIColor colorWithRed:0.961 green:0.349 blue:0.004 alpha:1.000];
    [[UIBarButtonItem appearanceWhenContainedIn: [UISearchBar class], nil] setTintColor:mainTextColor];
    [[UILabel       appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:mainTextColor];

    
    [[UITextField   appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:mainTextColor];
   
    CFURLRef clickSound = CFBundleCopyResourceURL(CFBundleGetMainBundle(),(__bridge CFStringRef)@"notification", CFSTR("wav"), NULL);
    AudioServicesCreateSystemSoundID (clickSound, &self->_soundID);
    CFRelease(clickSound);
    
    [self.window setTintColor:[UIColor colorWithRed:0.961 green:0.349 blue:0.004 alpha:1.000]];
    
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
															 bundle: nil];
    

	AGMenuViewController *leftMenu = (AGMenuViewController*)[mainStoryboard
                                                                 instantiateViewControllerWithIdentifier: @"AGMenuViewController"];
    
    [SlideNavigationController sharedInstance].leftMenu = leftMenu;
    [SlideNavigationController sharedInstance].view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    // Override point for customization after application launch.
    return YES;
}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    // Store the deviceToken in the current Installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}

-(void)applicationWillTerminate:(UIApplication *)application{
    AudioServicesDisposeSystemSoundID(self.soundID);
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive){
        [TWMessageBarManager sharedInstance].styleSheet = [AGStyleSheetForNotification styleSheet];
        NSString *desc = nil;
        @try {
            desc = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
        }
        @catch (NSException *exception) {
            desc = nil;
        }
        
        [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Ingame!"
                                                       description:desc
                                                              type:TWMessageBarMessageTypeSuccess];
        AudioServicesPlaySystemSound(self.soundID);
    }
}

@end
