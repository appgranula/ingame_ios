//
//  AGStyleSheetForNotification.m
//  ingame
//
//  Created by Eugene on 20/08/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import "AGStyleSheetForNotification.h"

@implementation AGStyleSheetForNotification

+ (AGStyleSheetForNotification *)styleSheet
{
    return [[AGStyleSheetForNotification alloc] init];
}


- (UIColor *)backgroundColorForMessageType:(TWMessageBarMessageType)type
{
    return [UIColor colorWithRed:43.0 / 255.0 green:43.0/255.0 blue:34.0/255.0 alpha:1.0];
}

- (UIColor *)strokeColorForMessageType:(TWMessageBarMessageType)type
{

  return [UIColor colorWithRed:43.0 / 255.0 green:43.0/255.0 blue:34.0/255.0 alpha:1.0];


}

- (UIImage *)iconImageForMessageType:(TWMessageBarMessageType)type
{
    return  [UIImage imageNamed:@"alertIcon"];
}

/*
- (UIFont *)titleFontForMessageType:(TWMessageBarMessageType)type
{
    return [UIFont fontWithName:@"AvenirNext-DemiBold" size:16.0f];
}

- (UIFont *)descriptionFontForMessageType:(TWMessageBarMessageType)type
{
    return [UIFont fontWithName:@"AvenirNext-Regular" size:14.0f];
}

- (UIColor *)titleColorForMessageType:(TWMessageBarMessageType)type
{
    return [UIColor blackColor];
}

- (UIColor *)descriptionColorForMessageType:(TWMessageBarMessageType)type
{
    return [UIColor purpleColor];
}
*/
@end
