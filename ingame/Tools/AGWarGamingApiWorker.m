//
//  AGWarGamingApiWorker.m
//  ingame
//
//  Created by Pavel Ivanov on 14/08/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import <Underscore.h>

#import "AGWarGamingApiWorker.h"
#import "AGWebViewController.h"

NSString* const AGWarGamingApiWorkerErrorDomain     = @"AGWarGamingApiWorkerErrorDomain";
NSString* const AGWarGamingBackendErrorDomain       = @"AGWarGamingBackendErrorDomain";
NSString* const AGWarGamingApiWorkerErrorRawData    = @"AGWarGamingApiWorkerErrorRawData";

NSString* const kAGWarGamingApiWorkerUserDidLogout  = @"kAGWarGamingApiWorkerUserDidLogout";

NSString* const kAGNSUserDefaultsWOTUser            = @"kAGNSUserDefaultsWOTUser";

AGGameType* const kAGGameTypeWorldOfTanks = @"wot";
AGGameType* const kAGGameTypeWorldOfWarplanes = @"wowp";

AGWOTUser* _wotUser;

typedef BFTask* (^AGRequestCursorNextCallback)(NSUInteger offset, NSUInteger size);

@interface AGRequestCursor() {
    NSArray*    _fetchedObjects;
    NSUInteger  _total;
    NSUInteger  _offset;
    NSUInteger  _loaded;
}
@property (copy, nonatomic) AGRequestCursorNextCallback nextCallback;
@property (strong, nonatomic) id info;

-(void)setTotal:(NSUInteger) total;
-(id)initWithNextBlock: (AGRequestCursorNextCallback) nextCallback;
@end

@implementation AGRequestCursor

-(id)initWithNextBlock:(AGRequestCursorNextCallback)nextCallback {
    NSParameterAssert(nextCallback);
    self = [super init];
    if (self) {
        self.nextCallback = nextCallback;
    }
    return self;
}

-(BFTask *)next {
    NSUInteger size = self.pageSize;
    BFTask* task = [[self nextCallback](self->_offset, size) continueWithBlock:^id(BFTask *task) {
        if (task.error) {
            self->_error = task.error;
            return [BFTask taskWithError:task.error];
        } else {
            self->_loaded += size;
            NSArray* newObjects = task.result;
            NSArray* objects = [NSArray arrayWithArray:self->_fetchedObjects];
            objects = [objects arrayByAddingObjectsFromArray:newObjects];
            self->_fetchedObjects = objects;
            return [BFTask taskWithResult:newObjects];
        }
    }];
    self->_offset += size;
    return task;
}

-(BOOL)isFinished {
    return self.error || self->_loaded >= self->_total;
}

-(void)setTotal:(NSUInteger)total {
    self->_total = total;
}

@end

@implementation NSError (BackendErrors)

- (NSError*)parse_explain {
    return nil;
}

+ (NSString*) wotNormalizedMessageCodeWithCode: (NSString *) code {
    static NSMutableDictionary* transformation;
    NSString* result = code;
    
    if (!transformation) {
        NSDictionary* strings = @{@".*_NOT_SPECIFIED"       : @"%FIELD%_NOT_SPECIFIED",
                                  @".*_NOT_FOUND"           : @"%FIELD%_NOT_FOUND",
                                  @".*_LIST_LIMIT_EXCEEDED" : @"%FIELD%_LIST_LIMIT_EXCEEDED",
                                  @"INVALID_.*"             : @"INVALID_%FIELD%"};
        transformation = [NSMutableDictionary dictionaryWithCapacity:strings.count];
        for (NSString* expression in strings) {
            NSError *error;
            NSRegularExpression* regExp = [NSRegularExpression regularExpressionWithPattern:expression
                                                                                    options:0
                                                                                      error:&error];
            if (error) {
                NSLog(@"Error while parsing WOT errors constants: %@", error);
                continue;
            }
            transformation[regExp] = strings[expression];
        }
    }
    
    for (NSRegularExpression* regExp in transformation) {
        NSTextCheckingResult* first = [regExp firstMatchInString:code
                                                         options:0
                                                           range:NSMakeRange(0, [code length])];
        if (first.range.location != NSNotFound) {
            result = transformation[regExp];
            break;
        }
    }
    return result;
}

+ (NSError*)errorFromWotResponse: (NSDictionary *) response {
    NSString* codeString    = response[@"code"];
    NSString* messageCode   = [NSError wotNormalizedMessageCodeWithCode: response[@"message"]];
    
    NSError* error = [NSError buildError:^(MRErrorBuilder *builder) {
        
        builder.debugDescriptionValue   = messageCode;
        builder.domain                  = AGWarGamingApiWorkerErrorDomain;
        builder.code                    = codeString.integerValue;
        [builder setUserInfoValue: response
                           forKey: AGWarGamingApiWorkerErrorRawData];

        NSString* format = NSLocalizedStringFromTable(messageCode, @"wot",
                                                      @"WOT API Error message: http://ru.wargaming.net/developers/documentation/guide/getting-started/. Possible placeholders: '%$1@' - response code number, '%$2@' - message, '%$3@' - involved field");
        format = [NSString stringWithFormat: NSLocalizedStringFromTable(messageCode, @"wot", @"WOT_SERVER_RESPOND_WITH_ERROR: %@"), format];
        NSString* fieldInfo = response[@"field"];
        if (response[@"value"]) {
            fieldInfo = [NSString stringWithFormat:@"%@='%@'", response[@"field"], response[@"value"]];
        }
        builder.localizedDescription = [NSString localizedStringWithFormat:format, fieldInfo];
    }];
    
    return error;
}
@end

@interface AGWarGamingApiWorker()
+(void)setWotUser:(AGWOTUser*)wotUser;
@end

@implementation AGWarGamingApiWorker

#pragma mark - Agregated backend support

+ (BFTask *) authenticateWithRegion: (AGWOTRegion *) region BeforeExecuteBlock:(void (^)())beforeExecuteBlock {
    return [[[[AGWebViewController authenticateWithRegion: region] continueWithSuccessBlock:^id(BFTask *task) {
        AGWOTUser* user         = [[AGWOTUser alloc] initWithAccessToken:task.result[@"access_token"] andAccountId:task.result[@"account_id"]];
        user.nickname           = task.result[@"nickname"];
        user.region             = region;
        user.game               = @"wot";
        user.token_expiresAt    = [NSDate dateWithTimeIntervalSince1970:[task.result[@"expires_at"] doubleValue]];
        [self setWotUser: user];
        beforeExecuteBlock();
        return [self.class parse_authenticateByUser:user];
    }] continueWithSuccessBlock:^id(BFTask *task) {
        NSDictionary* params = task.result;
        return [self parse_loginWithUsername:params[@"username"] password:params[@"password"]];
    }] continueWithSuccessBlock:^id(BFTask *task) {
        return [self parse_setNicknameToInstallation];
    }];
}

+ (BFTask *)logout {
    return [[self parse_logout] continueWithSuccessBlock:^id(BFTask *task) {
        [PFUser logOut];
        return [[self wot_logout] continueWithBlock:^id(BFTask *task) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kAGWarGamingApiWorkerUserDidLogout
                                                                object:nil];
            return nil;
        }];
    }];
}

+ (BFTask *)updateServerInformation {
    return [[self parse_updateInstallationStatus] continueWithBlock:^id(BFTask *task) {
        NSString* serverNickname = [PFInstallation currentInstallation][@"wotNickname"];
        NSString* localNickname  = [self wotUser].nicknameWithNamespace;
        if (localNickname && ![localNickname isEqualToString:serverNickname]) {
            NSLog(@"Local nickname '%@' is different then remote one '%@'. Installation droped from the server side. Logging out.", localNickname, serverNickname);
            return [[self wot_logout] continueWithBlock:^id(BFTask *task) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kAGWarGamingApiWorkerUserDidLogout
                                                                    object:nil];
                return task;
            }];
        }
        return task;
    }];
}

+ (BFTask *) fetchAllFriendsForGame:(AGGameType*)gameType {
    AGRequestCursor* cursor = [self wot_findFriendsForGame:gameType];
    cursor.pageSize = 100;
        
    BFContinuationBlock recursion;
    @weakify(cursor);
    
    recursion = ^BFTask*(BFTask* task) {
        @strongify(cursor);
        BFContinuationBlock recursion = (BFContinuationBlock)cursor.info;
        if (cursor.isFinished) {
            return [BFTask taskWithResult:cursor.fetchedObjects];
        } else {
            return [[cursor next] continueWithSuccessBlock:recursion];
        }
    };
    cursor.info = recursion;
    
    return [[[[cursor next] continueWithSuccessBlock:recursion]
             continueWithSuccessBlock:^id(BFTask *task) {
        return [[self parse_seekUsersInFriendsNameList:task.result] continueWithSuccessBlock:^id(BFTask *task) {
            NSArray* parseObjects = task.result;
            for (PFObject* object in parseObjects) {
                NSPredicate* predicate = [NSPredicate predicateWithFormat:@"nicknameWithNamespace == %@", object[@"wotNickname"]];
                AGWOTUser* friend = [cursor.fetchedObjects filteredArrayUsingPredicate:predicate].lastObject;
                friend.parseObject = object;
            }
            return [BFTask taskWithResult:cursor.fetchedObjects];
        }];
    }] continueWithBlock:^id(BFTask *task) {
        NSMutableArray *listOfFriendsNicknames = [NSMutableArray array];
        for (AGWOTUser *user in task.result) {
            [listOfFriendsNicknames addObject:user.nicknameWithNamespace];
        }
        

        NSString *key = [NSString stringWithFormat:@"%@Friends", gameType];
        [[PFUser currentUser] setObject:listOfFriendsNicknames
                                                       forKey:key];
        BFTaskCompletionSource *source = [BFTaskCompletionSource taskCompletionSource];
        [[PFUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (error) {
                error = [self parse_explainError:error];
                [source setError:error];
            } else {
                [source setResult:nil];
            }
        }];
        
        return task;
    }];
}

+ (AGRequestCursor *) wot_findFriendsForGame:(AGGameType*)gameType {
    __block NSArray* friends;
    __block NSNumber *clanId;
    AGWOTUser* wotUser = [self wotUser];
    
    __block AGRequestCursor* cursor = [[AGRequestCursor alloc] initWithNextBlock:^BFTask *(NSUInteger offset, NSUInteger size) {
        BFTask* loadFriends = [BFTask taskWithResult:nil];
        if (!friends) {
            NSString *userFields = @"account_id,private.friends";
            if ([gameType isEqualToString:kAGGameTypeWorldOfTanks]) {
                userFields = [userFields stringByAppendingString:@",clan_id"];
            }
            loadFriends = [[self wot_fetchUserInfoForGame:gameType ByUserAccoutId:wotUser.accountId andUserFields:userFields] continueWithSuccessBlock:^id(BFTask *task) {
                @try {
                    friends = [[[[task.result valueForKey:@"data"] valueForKey:[wotUser.accountId description]] valueForKey:@"private"] valueForKey:@"friends"];
                    clanId = [[[task.result valueForKey:@"data"] valueForKey:[wotUser.accountId description]] valueForKey:@"clan_id"];
                }
                @catch (NSException *exception) {
                    friends = nil;
                    clanId = nil;
                }
                
                if (clanId && ![clanId isKindOfClass:[NSNull class]]) {
                    wotUser.clanId = clanId;
                }
                
                friends = [friends isKindOfClass:[NSArray class]] ? friends : @[];
                [cursor setTotal: friends.count];
                return [BFTask taskWithResult:friends];
            }];
        }

        return [loadFriends continueWithSuccessBlock:^id(BFTask *task) {
            if (cursor.isFinished) {
                return [BFTask taskWithResult:nil];
            }

            NSRange range = NSMakeRange(offset, MIN(offset + size, friends.count));
            
            NSArray* friendsToFetch = [friends subarrayWithRange:range];
        
            return [[self wot_fetchUserInfoForGame:gameType ByMultipleUsers:friendsToFetch andUserFields:@"account_id,nickname"] continueWithSuccessBlock:^id(BFTask *task) {
                NSDictionary* response = task.result;
                NSDictionary* data = response[@"data"];
                NSMutableArray *friendsList;
                
                NSMutableArray *listOfFriendsNicknames = [NSMutableArray array];
                for (NSString* friend_id in data) {
                    NSDictionary *userInfo  = data[friend_id];
                    if (!userInfo || [userInfo isKindOfClass:[NSNull class]]) continue;
                    AGWOTUser *wotUser      = [AGWOTUser new];
                    wotUser.nickname        = userInfo[@"nickname"];
                    wotUser.region          = [self wotUser].region;
                    wotUser.game            = @"wot";
                    wotUser.accountId       = friend_id;
                    wotUser.lastBattleTime  = [[NSDate alloc] initWithTimeIntervalSince1970: [userInfo[@"last_battle_time"] doubleValue]];
                    
                    if (!friendsList) {
                        friendsList = [NSMutableArray array];
                    }
                    
                    [friendsList addObject:wotUser];
                    
                    [listOfFriendsNicknames addObject:wotUser.nicknameWithNamespace];
                }
                
                [AGWarGamingApiWorker wotUser].friends = listOfFriendsNicknames;
                return [BFTask taskWithResult:friendsList];
                
            }];
        }];
    }];
    return cursor;
}

+ (BFTask *) fetchAllClanMembers {
    return [[self wot_fetchClanFriendsList] continueWithBlock:^id(BFTask *task) {
        __block NSArray *friendList = task.result;
        return [[self parse_seekUsersInFriendsNameList:friendList] continueWithSuccessBlock:^id(BFTask *task) {
            NSArray* parseObjects = task.result;
            for (PFObject* object in parseObjects) {
                NSPredicate* predicate = [NSPredicate predicateWithFormat:@"nicknameWithNamespace == %@", object[@"wotNickname"]];
                AGWOTUser* friend = [friendList filteredArrayUsingPredicate:predicate].lastObject;
                friend.parseObject = object;
            }
            
            NSMutableArray *listOfClanMembersNicknames = [NSMutableArray array];
            for (AGWOTUser *user in friendList) {
                [listOfClanMembersNicknames addObject:user.nicknameWithNamespace];
            }
            
            NSString *key = [NSString stringWithFormat:@"wotClanMembers"];
            [[PFUser currentUser] setObject:listOfClanMembersNicknames
                                     forKey:key];
            BFTaskCompletionSource *source = [BFTaskCompletionSource taskCompletionSource];
            [[PFUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (error) {
                    error = [self parse_explainError:error];
                    [source setError:error];
                } else {
                    [source setResult:nil];
                }
            }];
            
            return [BFTask taskWithResult:friendList];
        }];

    }];
}

#pragma mark - WOT API
//TODO: удалить?
+ (BFTask *) wot_fetchFriendsListForGame: (AGGameType*)gameType
                               ByWOTUser: (AGWOTUser *)wot_user
                                 andPage: (NSInteger )page
{
    NSParameterAssert(wot_user);
    
    return [[[self wot_fetchUserInfoForGame:gameType ByUserAccoutId:wot_user.accountId andUserFields:nil] continueWithSuccessBlock:^id(BFTask *task)
             {
                 NSArray* friends;
                 
                 @try {
                     friends = [[[[task.result valueForKey:@"data"] valueForKey:[wot_user.accountId description]] valueForKey:@"private"] valueForKey:@"friends"];
                 }
                 @catch (NSException *exception) {
                     friends = nil;
                 }
                 
                 friends = [friends isKindOfClass:[NSArray class]] ? friends : @[];
                 
                 if (friends.count <= page * 100) {
                     return [BFTask taskWithResult:nil];
                 }
                 
                 NSRange range = NSMakeRange(page*100, MIN(100,friends.count - 100*page));
                 friends = [friends subarrayWithRange:range];
                 return [self wot_fetchUserInfoForGame:gameType ByMultipleUsers:friends andUserFields:nil];
             }] continueWithSuccessBlock:^id(BFTask *task) {
                 NSDictionary* response = task.result;
                 NSDictionary* data = response[@"data"];
                 NSMutableArray *friendsList;
                 
                 for (NSString* friend_id in data) {
                     NSDictionary *userInfo  = data[friend_id];
                     
                     AGWOTUser *wotUser      = [AGWOTUser new];
                     wotUser.nickname        = userInfo[@"nickname"];
                     wotUser.region          = [self wotUser].region;
                     wotUser.game            = @"wot";
                     wotUser.accountId       = friend_id;
                     wotUser.lastBattleTime  = [[NSDate alloc] initWithTimeIntervalSince1970: [userInfo[@"last_battle_time"] doubleValue]];
                     
                     if (!friendsList) {
                         friendsList = [NSMutableArray array];
                     }
                     
                     [friendsList addObject:wotUser];
                 }
                 
                 return [BFTask taskWithResult:friendsList];
                
             }];
}

+ (BFTask *) wot_fetchClanFriendsList
{
    return [[[self wot_fetchClanInfo] continueWithSuccessBlock:^id(BFTask *task)
             {
                 NSDictionary* friends;
                 
                 @try {
                     friends = [[[task.result valueForKey:@"data"] allValues][0] valueForKey:@"members"];
                 }
                 @catch (NSException *exception) {
                     friends = nil;
                 }
                 
                 friends = [friends isKindOfClass:[NSDictionary class]] ? friends : @{};
                 
                 
                 return [self wot_fetchUserInfoForGame:kAGGameTypeWorldOfTanks ByMultipleUsers:[friends allKeys] andUserFields:@"account_id,nickname"];
             }] continueWithBlock:^id(BFTask *task) {
                 NSDictionary* data = (NSDictionary*)task.result[@"data"];//response[@"data"];
                 NSMutableArray *friendsList;
                 
                 for (NSString* friend_id in data) {
                     NSDictionary *userInfo  = data[friend_id];
                     
                     AGWOTUser *wotUser      = [AGWOTUser new];
                     wotUser.nickname        = userInfo[@"nickname"];
                     wotUser.region          = [self wotUser].region;
                     wotUser.game            = @"wot";
                     wotUser.accountId       = friend_id;
                     wotUser.lastBattleTime  = [[NSDate alloc] initWithTimeIntervalSince1970: [userInfo[@"last_battle_time"] doubleValue]];
                     
                     if (!friendsList) {
                         friendsList = [NSMutableArray array];
                     }
                     
                     [friendsList addObject:wotUser];
                 }
                 
                 return [BFTask taskWithResult:friendsList];
             }];
}

+ (BFTask *) wot_fetchUserInfoForGame:(AGGameType*)gameType
                       ByUserAccoutId: (NSString *)user_accountId
                        andUserFields: (NSString *)user_fields{
    return [self wot_fetchUserInfoForGame:gameType ByMultipleUsers:@[user_accountId] andUserFields:user_fields];
}

+ (BFTask *) wot_fetchClanInfo {
    if (![self wotUser].clanId) {
        return [BFTask taskWithResult:nil];
    }
    NSDictionary *params = @{@"account_id": [self wotUser].accountId, @"clan_id":[self wotUser].clanId};
    return [self wot_makeAPIRequestWithMethod:@"GET" forGame:kAGGameTypeWorldOfTanks toResourse:@"wot/clan/info/" withParams:params];
}

+ (BFTask *) wot_fetchUserInfoForGame: (AGGameType*)gameType
                      ByMultipleUsers: (NSArray *)user_ids
                        andUserFields: (NSString *)user_fields{
    
    NSParameterAssert(user_ids);
    NSDictionary *params = @{@"account_id": [user_ids componentsJoinedByString:@", "]};
    
    if (user_fields) {
        params = Underscore.extend(@{@"fields": user_fields}, params);
    }
    return [self wot_makeAPIRequestWithMethod:@"GET" forGame:gameType toResourse:[NSString stringWithFormat:@"%@/account/info/", gameType] withParams:params];
}

+ (BFTask *) wot_logout {
    return [[self wot_makeAPIRequestWithMethod:@"POST" forGame:kAGGameTypeWorldOfTanks toResourse:@"wot/auth/logout/" withParams:nil] continueWithBlock:^id(BFTask *task) {
        [self setWotUser:nil];
        return task;
    }];
}

#pragma mark Foundation
+(AGWOTUser *)wotUser {
    @synchronized(_wotUser)
    {
        if (!_wotUser) {
            NSData* data = [[NSUserDefaults standardUserDefaults] valueForKey:kAGNSUserDefaultsWOTUser];
            NSKeyedUnarchiver *decoder = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
            decoder.requiresSecureCoding = YES;
            _wotUser = [decoder decodeObjectOfClass:[AGWOTUser class] forKey:NSKeyedArchiveRootObjectKey];
        }
    }
    return _wotUser;
}

+(void)setWotUser:(AGWOTUser *)wotUser {
    _wotUser = wotUser;
    NSData* data = [NSKeyedArchiver archivedDataWithRootObject:wotUser];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:kAGNSUserDefaultsWOTUser];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(NSString *)wot_applicationKeyForRegion:(AGWOTRegion *)region {
    
    static NSDictionary* keys;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        keys = @{kAGWOTRegionRU:    @"0ba576f27f3b0dc95cd56bf18733b1f1",
                 kAGWOTRegionASIA:  @"31660921fc05fd4a81c273238d16b9d2",
                 kAGWOTRegionEU:    @"ebf53a8e1d483cd16166e0538f5beff6",
                 kAGWOTRegionNA:    @"0391a11c5fa1966d5aed615427834e92",
                 kAGWOTRegionKR:    @"30d648c15db90131fc147c16a377d65e",
                 };
    });
    return keys[region];
}

+(NSURL *)wot_baseUrlForRegion:(AGWOTRegion *)region AndGameType:(AGGameType*)gameType{
    static NSDictionary* keys;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        keys = @{kAGGameTypeWorldOfTanks :
                     @{kAGWOTRegionRU:    @"https://api.worldoftanks.ru/",
                       kAGWOTRegionASIA:  @"https://api.worldoftanks.asia/",
                       kAGWOTRegionEU:    @"https://api.worldoftanks.eu/",
                       kAGWOTRegionNA:    @"https://api.worldoftanks.com/",
                       kAGWOTRegionKR:    @"https://api.worldoftanks.kr/",
                       },
                 kAGGameTypeWorldOfWarplanes :
                     @{kAGWOTRegionRU:    @"https://api.worldofwarplanes.ru/",
                       kAGWOTRegionASIA:  @"https://api.worldofwarplanes.asia/",
                       kAGWOTRegionEU:    @"https://api.worldofwarplanes.eu/",
                       kAGWOTRegionNA:    @"https://api.worldofwarplanes.com/",
                       kAGWOTRegionKR:    @"https://api.worldofwarplanes.kr/",
                       },
                 };
    });
    return [NSURL URLWithString:keys[gameType][region]];
}

+ (BFTask *) wot_makeAPIRequestWithMethod: (NSString *) method forGame:(AGGameType*)gameType toResourse: (NSString *) resourse withParams:(NSDictionary *)params {
    return [[self wot_updateAuthTokenIfNecessary] continueWithSuccessBlock:^id(BFTask *task)
            {
                return [[self wot_makeNudeAPIRequestWithMethod:method
                                                       forGame:gameType
                                                    toResourse:resourse
                                                    withParams:params] continueWithBlock:^id(BFTask *originalRequestTask)
                        {
                            if (task.error.code == 403) {
                                return [[self parse_logout] continueWithSuccessBlock:^id(BFTask *task) {
                                    [PFUser logOut];
                                    return [[self wot_logout] continueWithBlock:^id(BFTask *task) {
                                        [[NSNotificationCenter defaultCenter] postNotificationName:kAGWarGamingApiWorkerUserDidLogout
                                                                                            object:nil];
                                        return originalRequestTask;
                                    }];
                                }];
                            }
                            return originalRequestTask;
                        }];
            }];
}

+(BFTask*)wot_updateAuthTokenIfNecessary {
    
    if (![self wotUser]) {
        NSError* error = [NSError buildError:^(MRErrorBuilder *builder) {
            builder.localizedDescription = @"User is not authenticated in WOT server yet!";
            builder.code                 = AGWarGamingApiWorkerErrorCodeWOTAuthError;
            builder.domain               = AGWarGamingApiWorkerErrorDomain;
        }];
        return [BFTask taskWithError:error];
    }
    
    return [[BFTask taskWithResult:nil] continueWithSuccessBlock:^id(BFTask *task) {
        NSTimeInterval week = 60 * 60 * 24 * 7;
        NSTimeInterval interval = [[self wotUser].token_expiresAt timeIntervalSinceNow];
        if (interval > week) {
            return [BFTask taskWithResult:nil];
        } else {
            return [self wot_updateAccessToken];
        }
    }];
}

+(BFTask*)wot_updateAccessToken {
    return [[self wot_makeNudeAPIRequestWithMethod:@"POST"
                                           forGame:kAGGameTypeWorldOfTanks
                                        toResourse:@"wot/auth/prolongate/"
                                        withParams:nil] continueWithSuccessBlock:^id(BFTask *task)
            {
                NSDictionary* response = task.result;
                response = response[@"data"];
                
                @try {
                    NSParameterAssert(response[@"access_token"]);
                    NSParameterAssert(response[@"account_id"]);
                    NSParameterAssert(response[@"expires_at"]);
                }
                @catch (NSException *exception) {
                    MRErrorBuilder* builder = [MRErrorBuilder builderWithDomain:AGWarGamingApiWorkerErrorDomain
                                                                           code:AGWarGamingApiWorkerErrorCodeWOTMalformedRespose
                                                                      exception:exception];
                    builder.localizedDescription = exception.description;
                    return [BFTask taskWithError:builder.error];
                }
                
                AGWOTUser* user = [self wotUser];
                user.accessToken     = response[@"access_token"];
                user.accountId       = response[@"account_id"];
                user.token_expiresAt = [NSDate dateWithTimeIntervalSince1970:[response[@"expires_at"] doubleValue]];
                [self setWotUser:user];
                return [BFTask taskWithResult:nil];
            }];
}

+(BFTask*)wot_makeNudeAPIRequestWithMethod:(NSString*)method forGame:(AGGameType*)gameType toResourse: (NSString *) resourse withParams:(NSDictionary *)params {
    BFTaskCompletionSource *completionSource = [BFTaskCompletionSource taskCompletionSource];
    
    NSDictionary* finalParams = Underscore.extend(@{@"application_id": [self wot_applicationKeyForRegion:[self wotUser].region],
                                                    @"access_token":   [self wotUser].accessToken
                                                    }, params);
    
    NSURL* baseUrl  = [self wot_baseUrlForRegion:[self wotUser].region AndGameType:gameType];
    NSURL* url      = [NSURL URLWithString:resourse relativeToURL:baseUrl];
    
    AFHTTPRequestOperationManager* manager = [AFHTTPRequestOperationManager new];
    
    NSMutableURLRequest *request = [manager.requestSerializer requestWithMethod:method
                                                                      URLString:url.absoluteString
                                                                     parameters:finalParams
                                                                          error:nil];
    NSLog(@"WOT request: %@", request);
    AFHTTPRequestOperation* operation = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id response) {
        NSLog(@"WOT request successful: %@", operation.responseString);
        if ([response[@"status"] isEqualToString:@"error"]) {
            NSError* error = [NSError errorFromWotResponse:response[@"error"]];
            [completionSource setError:error];
        } else {
            [completionSource setResult:response];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"WOT request failed: %@", operation);
        if (operation.responseObject) {
            error = [NSError errorFromWotResponse:operation.responseObject];
        }
        [completionSource setError:error];
    }];
    [manager.operationQueue addOperation:operation];
    
    return completionSource.task;
}

#pragma mark - Parse backend API

+ (BFTask *) parse_authenticateByUser: (AGWOTUser *) currentUser{
    BFTaskCompletionSource *task = [BFTaskCompletionSource taskCompletionSource];
    [PFCloud callFunctionInBackground:@"wargamingAuth" withParameters:@{@"accessToken"  : currentUser.accessToken,
                                                                        @"accountId"    : currentUser.accountId,
                                                                        @"game"         : currentUser.game,
                                                                        @"region"       : currentUser.region}
                                block:^(id object, NSError *error) {
                                    if (error) {
                                        error = [self parse_explainError:error];
                                        [task setError:error];
                                    } else {
                                        [task setResult:object];
                                    }
                                }];
    return task.task;
}

+ (BFTask *) parse_updateInstallationStatus {
    if (![PFUser currentUser]) {
        return [BFTask taskWithResult:nil];
    }
    return [[self parse_fetchInstallation] continueWithSuccessBlock:^id(BFTask *task) {
        PFInstallation* installation = task.result;
        if (!installation) {
            return nil;
        }
        
        BFTaskCompletionSource *source = [BFTaskCompletionSource taskCompletionSource];
        installation[@"lastSeenAt"]  = [NSDate date];
        [installation saveEventually:^(BOOL succeeded, NSError *error) {
            if (error) {
                error = [self parse_explainError:error];
                [source setError:error];
            } else {
                [source setResult:installation];
            }
        }];
        return source.task;
    }];
}

+ (BFTask*) parse_fetchInstallation {
    PFInstallation* currentInstallation = [PFInstallation currentInstallation];
    
    if (!currentInstallation.objectId) {
        return [BFTask taskWithResult:nil];
    }
    
    BFTaskCompletionSource *task = [BFTaskCompletionSource taskCompletionSource];
    [currentInstallation fetchInBackgroundWithBlock:^(PFObject *currentInstallation, NSError *error) {
        if (error) {
            error = [self parse_explainError:error];
            [task setError:error];
        } else {
            [task setResult:currentInstallation];
        }
    }];
    return task.task;
}

+ (BFTask *)parse_loginWithUsername: (NSString *)username password: (NSString *)password {
    BFTaskCompletionSource *task = [BFTaskCompletionSource taskCompletionSource];
    [PFUser logInWithUsernameInBackground:username password:password block:^(PFUser *user, NSError *error) {
        if (error) {
            error = [self parse_explainError:error];
            [task setError:error];
        } else {
            [task setResult:user];
        }
    }];
    return task.task;
}

+ (BFTask *) parse_setNicknameToInstallation{
    BFTaskCompletionSource *task = [BFTaskCompletionSource taskCompletionSource];
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setObject:self.wotUser.nicknameWithNamespace forKey:@"wotNickname"];
    currentInstallation[@"lastSeenAt"]  = [NSDate date];
    [currentInstallation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (error) {
            error = [self parse_explainError:error];
            [task setError:error];
        } else {
            [task setResult:[BFTask taskWithResult:nil]];
        }
    }];
    return task.task;
}

+ (BFTask *) parse_seekUsersInFriendsNameList: (NSArray *)friendsNameList {
    
    BFTaskCompletionSource *task = [BFTaskCompletionSource taskCompletionSource];
    
    NSArray* allNicknames = Underscore.pluck(friendsNameList, @"nicknameWithNamespace");
    
    PFQuery *query = [PFUser query];
    [query whereKey:@"wotNickname" containedIn:allNicknames];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error){
            error = [self parse_explainError:error];
            [task setError:error];
        } else {
            [task setResult:objects];
        }
    }];
    
    return  task.task;
}

+ (BFTask *) parse_createIngameCounter:(AGWOTUser *)wotUser{
    BFTaskCompletionSource *task = [BFTaskCompletionSource taskCompletionSource];
    
    PFObject *counter       = [PFObject objectWithClassName:@"IngameCounter"];
    counter[@"owner"]       = [PFUser currentUser];
    counter[@"nickname"]    = wotUser.nicknameWithNamespace;
    counter[@"game"]        = @"wot";
    counter[@"count"]       = @1;
    
    PFACL *cACL = [PFACL ACL];
    [cACL setPublicReadAccess:YES];
    [cACL setWriteAccess:YES forUser:[PFUser currentUser]];
    counter.ACL = cACL;
    
    [counter saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (error) {
            error = [self parse_explainError:error];
            [task setError:error];
        } else {
            [task setResult:counter];
        }
    }];
    
    return  task.task;
}

+ (BFTask *) parse_findIngameCounter:(AGWOTUser *)wotUser {
    BFTaskCompletionSource *task = [BFTaskCompletionSource taskCompletionSource];
    PFQuery* query = [PFQuery queryWithClassName:@"IngameCounter"];
    [query whereKey:@"owner" equalTo:[PFUser currentUser]];
    [query whereKey:@"nickname" equalTo: wotUser.nicknameWithNamespace];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (error && error.code != 101) {
            error = [self parse_explainError:error];
            NSLog(@"E(%s: %d): %@", __PRETTY_FUNCTION__, __LINE__, error);
            [task setError:error];
        } else {
            [task setResult:object];
        }
    }];
    return task.task;
}

+ (BFTask *)parse_incrementIngameCounterFor:(AGWOTUser *)user{
    //TODO nsusers to array
    __block PFObject *counter = user.counter;
    if (!counter) {
        return [[[self parse_findIngameCounter:user] continueWithSuccessBlock:^id(BFTask *task) {
            if (task.result) {
                return task;
            }
            return [self parse_createIngameCounter:user];
        }] continueWithSuccessBlock:^id(BFTask *task) {
            user.counter = task.result;
            return task;
        }];
    } else {
        BFTaskCompletionSource *source = [BFTaskCompletionSource taskCompletionSource];
        [counter incrementKey:@"count"];
        [counter saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (error) {
                error = [self parse_explainError:error];
                NSLog(@"E(%s: %d): %@", __PRETTY_FUNCTION__, __LINE__, error);
                [source setError:error];
            } else {
                [source setResult:nil];
            }
        }];
        return source.task;
    }
}

+ (BFTask *) sendPushTo: (NSArray *)wot_users ForGame:(AGGameType*)gameType {
    return [[self parse_sendPushToWOTUser:wot_users ForGameType:gameType] continueWithSuccessBlock:^id(BFTask *task) {
        return nil;//[self parse_incrementIngameCounterFor:wot_users];
    }];
}

+ (BFTask *) parse_sendPushToWOTUser: (NSArray *)wot_users ForGameType:(AGGameType*)gameType{
    
    BFTaskCompletionSource *task = [BFTaskCompletionSource taskCompletionSource];
    NSMutableArray *nicknamesArray = [NSMutableArray array];
    for (AGWOTUser *user in wot_users) {
        [nicknamesArray addObject:user.nicknameWithNamespace];
    }
    
    NSString *gameString = [gameType isEqualToString:kAGGameTypeWorldOfTanks] ? @"World of Tanks" : @"World of Warplanes";
    NSString *message = [NSString stringWithFormat:@"%@ in %@", self.wotUser.nickname, gameString];
    NSString *game = gameType;
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:@{@"nicknames":nicknamesArray, @"message":message, @"game":game}];
    if ([AGWarGamingApiWorker wotUser].clanId) {
        
    }
    
    [PFCloud callFunctionInBackground:@"sendPush" withParameters:@{@"nicknames":nicknamesArray, @"message":message, @"game":game} block:^(id object, NSError *error) {
        if (error) {
            error = [self parse_explainError:error];
            [task setError:error];
        } else {
            [task setResult:object];
        }
    }];
    
    return task.task;
}

+ (BFTask *) parse_logout {
    BFTaskCompletionSource *task = [BFTaskCompletionSource taskCompletionSource];
    
    [PFCloud callFunctionInBackground:@"logout" withParameters:@{} block:^(id object, NSError *error) {
        if (error) {
            error = [self parse_explainError:error];
            [task setError:error];
        } else {
            [task setResult:object];
        }
    }];
    
    return task.task;
}

+(NSError *)parse_explainError: (NSError *)error {
    
    if (error.userInfo[@"error"]) {
        NSString* detailsString = error.userInfo[@"error"];
        NSData* data = [detailsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary* details = [NSJSONSerialization JSONObjectWithData:data
                                                        options: 0
                                                          error: nil];
        
        if (details) {
            if (![details isKindOfClass:[NSDictionary class]]) {
                return error;
            }
            return [NSError buildError:^(MRErrorBuilder *builder) {
                builder.domain = AGWarGamingBackendErrorDomain;
                builder.code = ((NSNumber*)details[@"code"]).integerValue;
                builder.localizedDescription = details[@"message"];
                builder.underlyingError = error;
            }];
        }
        
        @try {
            NSArray* parts = [detailsString componentsSeparatedByString:@"{"];
            if (parts.count == 2) {
                NSArray* equationStrings = [parts[0] componentsSeparatedByString:@" "];
                NSMutableDictionary* info = Underscore.array(equationStrings).reduce([NSMutableDictionary dictionary],
                                                                                     ^(NSMutableDictionary* acc, NSString *part)
                                                                                     {
                                                                                         NSArray *tokens = [part componentsSeparatedByString:@"="];
                                                                                         if (tokens.count == 2) {
                                                                                             NSString* key = tokens[0];
                                                                                             NSString* value = tokens[1];
                                                                                             acc[key] = value;
                                                                                         }
                                                                                         return acc;
                                                                                     });
                
                NSString* objectString = parts[1];
                
                NSString* encodedDict = [objectString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"}"]];
                equationStrings = [encodedDict componentsSeparatedByString:@", NS"];
                info = Underscore.array(equationStrings).reduceRight(info,
                                                                ^(NSMutableDictionary* acc, NSString *part)
                                                                {
                                                                    NSArray *tokens = [part componentsSeparatedByString:@"="];
                                                                    if (tokens.count == 2) {
                                                                        NSString* key = tokens[0];
                                                                        NSString* value = tokens[1];
                                                                        acc[[@"NS" stringByAppendingString: key]] = value;
                                                                    }
                                                                    return acc;
                                                                });

                if (info[@"Domain"] && info[@"Code"]) {
                    
                    error = [NSError buildError:^(MRErrorBuilder *builder) {
                        NSString* codeString = info[@"Code"];
                        builder.code = codeString.integerValue;
                        builder.domain = info[@"Domain"];
                        builder.underlyingError = error;
                        NSArray* cutoff = [objectString componentsSeparatedByString:@"\""];
                        if (cutoff.count >= 3) {
                            NSString* text = cutoff[cutoff.count-2];
                            if ([text isKindOfClass:[NSString class]]) {
                                builder.localizedDescription = text;
                            }
                        }
                    }];
                }
            }
        }
        @catch (NSException *exception) {
        }
    }
    
    return error;
}

+(NSDictionary*) groupsForGame:(NSString*)gameType inList:(AGListType)listType {
    PFObject* parseObject = [PFUser currentUser];
    if (!parseObject) {
        NSLog(@"Error: Parse object for current user is not defined");
        return nil;
    }
    
    NSString *key = [NSString stringWithFormat:@"%@%@Groups", gameType, listType == AGListTypeFriends?@"":@"Clan"];
    NSArray* array = parseObject[key];
    NSMutableDictionary* groups = Underscore.array(array).reduce([NSMutableDictionary dictionary], ^(NSMutableDictionary* acc, NSString* string){
        NSRange range = [string rangeOfString:@"."];
        NSString* login = [string substringToIndex:range.location];
        NSString* group = [string substringFromIndex:range.location+1];
        
        NSMutableArray* groupmates = acc[group];
        groupmates = groupmates ? groupmates : [NSMutableArray array];
        [groupmates addObject:login];
        acc[group] = groupmates;
        
        return acc;
    });
    
    return groups;
}

#pragma mark - Group methods

+(void) addUsers:(NSArray*)logins toGroup: (NSString*)name inList:(AGListType)listType forGame:(NSString *)gameType {
    PFObject* parseObject = [PFUser currentUser];
    if (!parseObject) {
        NSLog(@"Error: Parse object for current user is not defined");
        return;
    }
    
    NSArray* packed = Underscore.array(logins).map(^(NSString* user){
        return [@[user, name] componentsJoinedByString:@"."];
    }).unwrap;
    
    NSString *key = [NSString stringWithFormat:@"%@%@Groups", gameType, listType == AGListTypeFriends?@"":@"Clan"];
    [parseObject addUniqueObjectsFromArray:packed forKey:key];
}

+(void) removeUsers:(NSArray*)logins fromGroup: (NSString*)name inList:(AGListType)listType forGame:(NSString *)gameType {
    PFObject* parseObject = [PFUser currentUser];
    if (!parseObject) {
        NSLog(@"Error: Parse object for current user is not defined");
        return;
    }
    
    NSArray* packed = Underscore.array(logins).map(^(NSString* user){
        return [@[user, name] componentsJoinedByString:@"."];
    }).unwrap;
    
    NSString *key = [NSString stringWithFormat:@"%@%@Groups", gameType, listType == AGListTypeFriends?@"":@"Clan"];
    [parseObject removeObjectsInArray:packed forKey:key];
}

+(void) removeGroup:(NSString*)name inList:(AGListType)listType forGame:(NSString *)gameType {
    PFObject* parseObject = [PFUser currentUser];
    if (!parseObject) {
        NSLog(@"Error: Parse object for current user is not defined");
        return;
    }
    
    NSArray* logins = [self groupsForGame:gameType inList:listType][name];
    
    NSArray* packed = Underscore.array(logins).map(^(NSString* user){
        return [@[user, name] componentsJoinedByString:@"."];
    }).unwrap;
    
    NSString *key = [NSString stringWithFormat:@"%@%@Groups", gameType, listType == AGListTypeFriends?@"":@"Clan"];
    [parseObject removeObjectsInArray:packed forKey:key];
}

#pragma mark - Watching methods

+(NSArray*) watchList {
    return [PFUser currentUser][@"watchList"];
}

+(void) watchUsers: (NSArray*) users {
    PFObject* parseObject = [PFUser currentUser];
    if (!parseObject) {
        NSLog(@"Error: Parse object for current user is not defined");
        return;
    }
    
    [parseObject addUniqueObjectsFromArray:users forKey:@"watchList"];
}
+(void) unwatchUsers: (NSArray*) users {
    PFObject* parseObject = [PFUser currentUser];
    if (!parseObject) {
        NSLog(@"Error: Parse object for current user is not defined");
        return;
    }
    
    [parseObject removeObjectsInArray:users forKey:@"watchList"];
}

#pragma mark - Parse Save & Refresh

+(BFTask*) parse_save {
    PFObject* parseObject = [PFUser currentUser];
    if (!parseObject) {
        NSLog(@"Error: Parse object for current user is not defined");
        return [BFTask taskWithResult:nil];
    }
    
    BFTaskCompletionSource* source = [BFTaskCompletionSource taskCompletionSource];
    
    [parseObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (error) {
            error = [self parse_explainError:error];
            [source setError:error];
        } else {
            [source setResult:nil];
        }
    }];
    
    return source.task;
}

+(BFTask*) parse_refresh {
    PFObject* parseObject = [PFUser currentUser];
    if (!parseObject) {
        NSLog(@"Error: Parse object for current user is not defined");
        return [BFTask taskWithResult:nil];
    }
    
    BFTaskCompletionSource* source = [BFTaskCompletionSource taskCompletionSource];
    
    [parseObject fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (error) {
            error = [self parse_explainError:error];
            [source setError:error];
        } else {
            [source setResult:nil];
        }
    }];
    
    return source.task;
}

@end
