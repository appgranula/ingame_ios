//
//  AGWarGamingApiWorker.h
//  ingame
//
//  Created by Pavel Ivanov on 14/08/14.
//  Copyright (c) 2014 Eugene. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AGWOTUser.h"

extern NSString* const AGWarGamingApiWorkerErrorDomain;
extern NSString* const AGWarGamingBackendErrorDomain;
extern NSString* const AGWarGamingApiWorkerErrorRawData;

extern NSString* const kAGWarGamingApiWorkerUserDidLogout;

typedef NSString AGGameType;

extern AGGameType* const kAGGameTypeWorldOfTanks;
extern AGGameType* const kAGGameTypeWorldOfWarplanes;

typedef NS_ENUM(NSUInteger, AGWarGamingApiWorkerErrorCode) {
    AGWarGamingApiWorkerErrorCodeUnknownError = 0,
    AGWarGamingApiWorkerErrorCodeWOTUnknownError,
    AGWarGamingApiWorkerErrorCodeWOTAuthCanceledByUser = 401,
    AGWarGamingApiWorkerErrorCodeWOTAuthRequestTimeout,
    AGWarGamingApiWorkerErrorCodeWOTAuthError,
    AGWarGamingApiWorkerErrorCodeWOTMalformedRespose,
    AGWarGamingApiWorkerErrorCodeWOTFieldNotSpecified,
    AGWarGamingApiWorkerErrorCodeWOTFieldNotFound,
    AGWarGamingApiWorkerErrorCodeWOTMethodNotFound,
    AGWarGamingApiWorkerErrorCodeWOTMethodDisabled,
    AGWarGamingApiWorkerErrorCodeWOTFieldListLimitExceeded,
    AGWarGamingApiWorkerErrorCodeWOTAccountIdListLimitExceeded,
    AGWarGamingApiWorkerErrorCodeWOTApplicationIsBlocked,
    AGWarGamingApiWorkerErrorCodeWOTInvalidField,
    AGWarGamingApiWorkerErrorCodeWOTInvalidApplicationId,
    AGWarGamingApiWorkerErrorCodeWOTInvalidIpAddress,
    AGWarGamingApiWorkerErrorCodeWOTRequestLimitExceeded,
    AGWarGamingApiWorkerErrorCodeWOTSourceNotAvalible,
};

typedef NS_ENUM(NSUInteger, AGListType) {
    AGListTypeFriends,
    AGListTypeClan
};

@interface NSError (BackendErrors)
- (NSError*)parse_explain;
+ (NSError*)errorFromWotResponse: (NSDictionary *) response;
@end

@interface AGRequestCursor : NSObject

@property (readonly, nonatomic) NSArray     *fetchedObjects;

@property (readonly, nonatomic) NSUInteger  total;
@property (readonly, nonatomic) NSUInteger  offset;
@property (assign,   nonatomic) NSUInteger  pageSize;

@property (readonly, nonatomic) BOOL        isFinished;
@property (readonly, nonatomic) NSError     *error;

- (BFTask *) next;
@end

@interface AGWarGamingApiWorker : NSObject

+ (AGWOTUser*)  wotUser;
+ (NSString*)   wot_applicationKeyForRegion:    (AGWOTRegion *)region;
+ (NSURL *)     wot_baseUrlForRegion:           (AGWOTRegion *)region AndGameType:(AGGameType*)gameType;

+ (NSDictionary*) groupsForGame:(AGGameType*)gameType                     inList:(AGListType)listType;
+ (void) addUsers:      (NSArray*)logins      toGroup: (NSString*)name    inList:(AGListType)listType forGame:(AGGameType*)gameType;
+ (void) removeUsers:   (NSArray*)logins    fromGroup: (NSString*)name    inList:(AGListType)listType forGame:(AGGameType*)gameType;
+ (void) removeGroup:                                  (NSString*)name    inList:(AGListType)listType forGame:(AGGameType*)gameType;

+ (NSArray*) watchList;
+ (void) watchUsers:    (NSArray*) users;
+ (void) unwatchUsers:  (NSArray*) users;

+ (BFTask *) parse_save;
+ (BFTask *) parse_refresh;

+ (BFTask *) authenticateWithRegion: (AGWOTRegion *) region BeforeExecuteBlock:(void (^)())beforeExecuteBlock;
+ (BFTask *) updateServerInformation;
+ (BFTask *) logout;
+ (BFTask *) fetchAllFriendsForGame:(AGGameType*)gameType;
+ (BFTask *) sendPushTo: (NSArray *)wot_users ForGame:(AGGameType*)gameType;
+ (BFTask *) parse_updateInstallationStatus;

+ (BFTask *) wot_fetchClanInfo;
+ (BFTask *) fetchAllClanMembers;

@end
