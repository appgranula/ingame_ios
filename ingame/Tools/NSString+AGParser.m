//
//  NSString+AGParser.m
//  ingame
//
//  Created by Pavel Ivanov on 29/09/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import "NSString+AGParser.h"

@implementation NSString (AGParser)

+(NSString*)getUserNameFromFullNickname:(NSString*)fullNickname
{
    return [fullNickname componentsSeparatedByString:@"_"].lastObject;
}

@end
