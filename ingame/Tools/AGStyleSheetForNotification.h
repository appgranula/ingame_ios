//
//  AGStyleSheetForNotification.h
//  ingame
//
//  Created by Eugene on 20/08/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TWMessageBarManager.h>

@interface AGStyleSheetForNotification : NSObject <TWMessageBarStyleSheet>
+ (AGStyleSheetForNotification *)styleSheet;
@end
