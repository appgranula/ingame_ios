//
//  NSString+AGParser.h
//  ingame
//
//  Created by Pavel Ivanov on 29/09/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (AGParser)

+(NSString*)getUserNameFromFullNickname:(NSString*)fullNickname;

@end
