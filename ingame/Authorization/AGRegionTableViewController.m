//
//  AGRegionTableViewController.m
//  ingame
//
//  Created by Pavel Ivanov on 19/08/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import "AGRegionTableViewController.h"
#import "AGWOTUser.h"
#import "AGWarGamingApiWorker.h"
#import <SVProgressHUD.h>

@interface AGRegionTableViewController ()
@property (nonatomic, strong) NSString *region;
@end

@implementation AGRegionTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.clearsSelectionOnViewWillAppear = YES;
    if ([AGWarGamingApiWorker wotUser])
    {
        NSLog(@"Current wot_user have been found! Go to Friend's List Controller");
        [self performSegueWithIdentifier:@"List" sender:self];
    } else {
        NSLog(@"Can't find current wot_user! Press 'Ingame!' button for authentication");
    }
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kAGWarGamingApiWorkerUserDidLogout
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *note) {
                                                      if (self.navigationController) {
                                                          [self.navigationController popToRootViewControllerAnimated:YES];
                                                      }
                                                  }];

}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.translucent = YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.tableView deselectRowAtIndexPath:self.tableView.indexPathForSelectedRow animated:YES];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            self.region = kAGWOTRegionRU;
            break;
        case 1:
            self.region = kAGWOTRegionEU;
            break;
        case 2:
            self.region = kAGWOTRegionNA;
            break;
        case 3:
            self.region = kAGWOTRegionASIA;
            break;
        case 4:
            self.region = kAGWOTRegionKR;
            break;
        default:
            break;
    }
    

    [[AGWarGamingApiWorker authenticateWithRegion:self.region BeforeExecuteBlock:^{
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    }] continueWithBlock:^id(BFTask *task) {
        [SVProgressHUD dismiss];
        if (task.error && !task.error.isCancelledError) {
            [self.tableView presentError:task.error];
        } else {
            [self performSegueWithIdentifier:@"List" sender:self];
        }
        return nil;
    }];
}
@end
