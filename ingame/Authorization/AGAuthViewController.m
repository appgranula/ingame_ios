//
//  AGAuthViewController.m
//  ingame
//
//  Created by Eugene on 13/08/14.
//  Copyright (c) 2014 Eugene. All rights reserved.
//

#import "AGAuthViewController.h"
#import "AGWarGamingApiWorker.h"

@interface AGAuthViewController ()<UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property NSString * access_token;
@property NSString * account_id;
@property BFTaskCompletionSource* resultTaskCompletionSource;
@property AGWOTRegion* region;

@end

@implementation AGAuthViewController

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.resultTaskCompletionSource = [BFTaskCompletionSource taskCompletionSource];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSURL* baseUrl          = [AGWarGamingApiWorker wot_baseUrlForRegion:self.region AndGameType:kAGGameTypeWorldOfTanks];
    NSString* appKey        = [AGWarGamingApiWorker wot_applicationKeyForRegion:self.region];
    NSString* relativeUrl   = [NSString stringWithFormat:@"wot/auth/login/?application_id=%@&redirect_uri=http://appgranula.com", appKey];
    NSURL* url              = [NSURL URLWithString:relativeUrl relativeToURL:baseUrl];
    NSURLRequest* request   = [NSURLRequest requestWithURL:url];
    
    [self.webView loadRequest:request];
    self.webView.delegate = self;
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    if ([error.domain isEqualToString:@"WebKitErrorDomain"]) {
        return;
    }
    if ([error.domain isEqualToString:NSURLErrorDomain] && error.code == kCFURLErrorCancelled) {
        return;
    }
    
    if (![error.domain isEqualToString:@"WebKitErrorDomain"] ) {
        [webView presentError:error];
    }
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {

    if ([request.URL.host isEqualToString:@"appgranula.com"]) {
        NSString* query = request.URL.query;
        NSArray* params = [query componentsSeparatedByString:@"&"];
        NSMutableDictionary* response = [NSMutableDictionary dictionary];
        for (NSString* param in params) {
            NSArray* parts = [param componentsSeparatedByString:@"="];
            if (parts.count == 2) {
                response[parts[0]] = parts[1];
            }
        }

        [self dismissViewControllerAnimated:YES completion:^{
            if ([response[@"status"] isEqualToString:@"error"]) {
                NSError* error = [NSError errorFromWotResponse:response];
                [self.resultTaskCompletionSource setError: error];
            } else {
                
                @try {
                    NSParameterAssert(response[@"account_id"]);
                    NSParameterAssert(response[@"access_token"]);
                    NSParameterAssert(response[@"expires_at"]);
                    NSParameterAssert(response[@"nickname"]);
                    [self.resultTaskCompletionSource setResult:response];
                }
                @catch (NSException *exception) {
                    MRErrorBuilder* builder = [MRErrorBuilder builderWithDomain:AGWarGamingApiWorkerErrorDomain
                                                                           code:AGWarGamingApiWorkerErrorCodeWOTMalformedRespose
                                                                      exception:exception];
                    builder.localizedDescription = exception.description;
                    [self.resultTaskCompletionSource setError:builder.error];
                }
            }
        }];
        return NO;
    }
    return YES;
}

+(BFTask *)authenticateWithRegion: (AGWOTRegion *)region {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[NSBundle mainBundle].infoDictionary[@"UIMainStoryboardFile"]
                                                         bundle:[NSBundle mainBundle]];
    UINavigationController *navvc = [storyboard instantiateViewControllerWithIdentifier:@"AuthNavigationController"];
    AGAuthViewController *controller = navvc.viewControllers[0];
    controller.region = region;
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:navvc
                                                                                     animated:YES
                                                                                   completion:nil];
    return controller.resultTaskCompletionSource.task;
}

- (IBAction)closeButtonTap:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        NSError* error = [NSError buildError:^(MRErrorBuilder *builder) {
            builder.domain = NSCocoaErrorDomain;
            builder.code = NSUserCancelledError;
            builder.localizedDescription = @"User canceled authentication with 'Close' button";
        }];
        [self.resultTaskCompletionSource setError:error];
    }];
}

@end
