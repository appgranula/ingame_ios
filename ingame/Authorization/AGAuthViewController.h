//
//  AGAuthViewController.h
//  ingame
//
//  Created by Eugene on 13/08/14.
//  Copyright (c) 2014 Eugene. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AGWOTUser.h"

@protocol authenticationDelegate <NSObject>
- (void)didAuthentication: (NSString* ) access_token and:(NSString *)account_id;
@end


@interface AGAuthViewController : UIViewController
@property id <authenticationDelegate>  authDelegate;

+ (BFTask *)authenticateWithRegion: (AGWOTRegion *)region;
- (IBAction)closeButtonTap:(id)sender;


@end
