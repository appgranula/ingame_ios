//
//  AGWebViewController.m
//  ingame
//
//  Created by Pavel Ivanov on 25/08/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import "AGWebViewController.h"
#import <NJKWebViewProgress/NJKWebViewProgress.h>
#import <NJKWebViewProgressView.h>
#import "AGWarGamingApiWorker.h"

@interface AGWebViewController () <UIWebViewDelegate, NJKWebViewProgressDelegate>
@property (nonatomic, strong) NJKWebViewProgress *progressProxy;
@property (nonatomic, strong) NJKWebViewProgressView *progressView;
@property NSString * access_token;
@property NSString * account_id;
@property BFTaskCompletionSource* resultTaskCompletionSource;
@property AGWOTRegion* region;

@end

@implementation AGWebViewController

#pragma mark - initialization

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.resultTaskCompletionSource = [BFTaskCompletionSource taskCompletionSource];
    }
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setRightButtonToRefresh:YES];
    self.leftBarButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"backbutton"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTap:)];
    self.navigationItem.leftBarButtonItem = self.leftBarButton;
    self.navigationItem.title = NSLocalizedString(@"Login", @"Login");
    
    _progressProxy = [[NJKWebViewProgress alloc] init];
    self.webView.delegate = _progressProxy;
    _progressProxy.webViewProxyDelegate = self;
    _progressProxy.progressDelegate = self;
    _progressView = [[NJKWebViewProgressView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    
    NSURL* baseUrl          = [AGWarGamingApiWorker wot_baseUrlForRegion:self.region AndGameType:kAGGameTypeWorldOfTanks];
    NSString* appKey        = [AGWarGamingApiWorker wot_applicationKeyForRegion:self.region];
    NSString* relativeUrl   = [NSString stringWithFormat:@"wot/auth/login/?application_id=%@&redirect_uri=http://appgranula.com", appKey];
    NSURL* url              = [NSURL URLWithString:relativeUrl relativeToURL:baseUrl];
    NSURLRequest* request   = [NSURLRequest requestWithURL:url];
    
    [self.webView loadRequest:request];
    self.progressView.translatesAutoresizingMaskIntoConstraints = NO;

//    self.webView.translatesAutoresizingMaskIntoConstraints = NO;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.view addSubview:_progressView];
}

-(void)updateViewConstraints
{
    [super updateViewConstraints];
    NSDictionary *viewsDictionary = @{@"progressView" : self.progressView};
    [self.progressView removeConstraints:self.progressView.constraints];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-64-[progressView]"
                                                                      options:0
                                                                      metrics:nil
//                                                                        views:@{@"progressView" : self.progressView, @"webView":self.webView}]];
                                                                        views:viewsDictionary]];
    
    [self.progressView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[progressView(==320)]"
                                                                              options:0
                                                                              metrics:nil
                                                                                views:viewsDictionary]];
    
    
    [self.progressView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[progressView(==2)]"
                                                                              options:0
                                                                              metrics:nil
                                                                                views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[progressView]"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:viewsDictionary]];
    
}

#pragma mark - NJKWebViewProgressDelegate

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    if ([request.URL.host isEqualToString:@"appgranula.com"]) {
        NSString* query = request.URL.query;
        NSArray* params = [query componentsSeparatedByString:@"&"];
        NSMutableDictionary* response = [NSMutableDictionary dictionary];
        for (NSString* param in params) {
            NSArray* parts = [param componentsSeparatedByString:@"="];
            if (parts.count == 2) {
                response[parts[0]] = parts[1];
            }
        }
        
        if ([response[@"status"] isEqualToString:@"error"]) {
        [self.navigationController popViewControllerAnimated:YES];
            NSError* error = [NSError errorFromWotResponse:response];
            [self.resultTaskCompletionSource setError: error];
        } else {
            
            @try {
                NSParameterAssert(response[@"account_id"]);
                NSParameterAssert(response[@"access_token"]);
                NSParameterAssert(response[@"expires_at"]);
                NSParameterAssert(response[@"nickname"]);
                [self.resultTaskCompletionSource setResult:response];
            }
            @catch (NSException *exception) {
                [self.navigationController popViewControllerAnimated:YES];
                MRErrorBuilder* builder = [MRErrorBuilder builderWithDomain:AGWarGamingApiWorkerErrorDomain
                                                                       code:AGWarGamingApiWorkerErrorCodeWOTMalformedRespose
                                                                  exception:exception];
                builder.localizedDescription = exception.description;
                [self.resultTaskCompletionSource setError:builder.error];
            }
        }
        return NO;
    }
    return YES;
}

-(void)webViewProgress:(NJKWebViewProgress *)webViewProgress updateProgress:(float)progress
{
    [_progressView setProgress:progress animated:YES];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self setRightButtonToRefresh:YES];
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [self setRightButtonToRefresh:NO];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    if ([error.domain isEqualToString:@"WebKitErrorDomain"]) {
        return;
    }
    if ([error.domain isEqualToString:NSURLErrorDomain] && error.code == kCFURLErrorCancelled) {
        return;
    }
    
    if (![error.domain isEqualToString:@"WebKitErrorDomain"] ) {
        [self.webView presentError:error];
    }
}

#pragma mark - Actions

- (IBAction)reloadTap:(id)sender {
    if (_webView.isLoading) {
        [_webView stopLoading];
        return;
    }
    
    [_webView reload];
}

- (IBAction)backButtonTap:(id)sender {
    if (_webView.canGoBack) {
        [_webView goBack];
        return;
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    NSError* error = [NSError buildError:^(MRErrorBuilder *builder) {
        builder.domain = AGWarGamingApiWorkerErrorDomain;
        builder.code = AGWarGamingApiWorkerErrorCodeWOTAuthCanceledByUser;
        builder.localizedDescription = @"User canceled authentication with 'Close' button";
    }];
    [self.resultTaskCompletionSource setError:error];
}

#pragma mark - Private Methods

-(void)setRightButtonToRefresh:(BOOL)needToSetToRefresh
{
    self.rightBarButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:needToSetToRefresh?UIBarButtonSystemItemRefresh:UIBarButtonSystemItemStop target:self action:@selector(reloadTap:)];
    self.navigationItem.rightBarButtonItem = self.rightBarButton;
}

#pragma mark - Static Public Methods

+(BFTask *)authenticateWithRegion: (AGWOTRegion *)region {
    UINavigationController *topNavigationController = [self topNavigationViewController];
    if (!topNavigationController) {
        return nil;
    }
    
    AGWebViewController *controller = [[AGWebViewController alloc] initWithNibName:@"AGWebView" bundle:[NSBundle mainBundle]];
    controller.region = region;
    [topNavigationController pushViewController:controller animated:YES];
    return controller.resultTaskCompletionSource.task;
}

#pragma mark - Static Private Methods

+ (UINavigationController *)topNavigationViewController{
    UIViewController *topVC = [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
    if ([topVC isKindOfClass:[UINavigationController class]]) {
        return (UINavigationController*)topVC;
    }
    
    if (topVC.navigationController) {
        return topVC.navigationController;
    }
    
    return nil;
}

+ (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isMemberOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [self topViewController:lastViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self topViewController:presentedViewController];
}

@end
