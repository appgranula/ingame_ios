//
//  AGWebViewController.h
//  ingame
//
//  Created by Pavel Ivanov on 25/08/14.
//  Copyright (c) 2014 Appgranula. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AGWOTUser.h"

@protocol authenticationDelegate <NSObject>
- (void)didAuthentication: (NSString* ) access_token and:(NSString *)account_id;
@end


@interface AGWebViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIBarButtonItem *rightBarButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *leftBarButton;
- (IBAction)reloadTap:(id)sender;
- (IBAction)backButtonTap:(id)sender;
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@property id <authenticationDelegate>  authDelegate;

+ (BFTask *)authenticateWithRegion: (AGWOTRegion *)region;

@end
